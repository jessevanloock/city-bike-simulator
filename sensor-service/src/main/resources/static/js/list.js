let url = '';
let list_data = [];
let coordX = 0;
let coordY = 0;

filterData();

function filterData(){
    url = 'http://localhost:8082/api/list_data';
    let typeMeting = document.querySelector('input[name="type"]:checked').value;
    let startDate = document.querySelector('input[name="startdate"]').value;
    let endDate = document.querySelector('input[name="enddate"]').value;
    let distance = document.querySelector('input[name="distance"]').value;
    url += "?type="+typeMeting+"&startdate="+startDate+"&enddate="+endDate+"&distance="+distance+"&coordX="+coordX+"&coordY="+coordY;
    fetchData();
    return false;
}

async function fetchData() {
    const response = await fetch(url);
    const myJson = await response.json();
    console.log(JSON.stringify(myJson));

    list_data = JSON.parse(JSON.stringify(myJson));
    let table = document.getElementById("table");
    while(table.hasChildNodes())
    {
        table.removeChild(table.firstChild);
    }
    let row = table.insertRow(0);
    let cell1 = row.insertCell(0);
    cell1.innerHTML = "ID";
    let cell2 = row.insertCell(1);
    cell2.innerHTML = "TimeStamp";
    let cell3 = row.insertCell(2);
    cell3.innerHTML = "X-Coord";
    let cell4 = row.insertCell(3);
    cell4.innerHTML = "Y-Coord";
    let cell5 = row.insertCell(4);
    cell5.innerHTML = "Type";
    let cell6 = row.insertCell(5);
    cell6.innerHTML = "Value";
    for (let i = 0; i < list_data.length; i++) {
        row = table.insertRow(i + 1);
        cell1 = row.insertCell(0);
        cell1.innerHTML = list_data[i].id;
        cell2 = row.insertCell(1);
        cell2.innerHTML = list_data[i].timeStamp;
        cell3 = row.insertCell(2);
        cell3.innerHTML = list_data[i].xCoord;
        cell4 = row.insertCell(3);
        cell4.innerHTML = list_data[i].yCoord;
        cell5 = row.insertCell(4);
        cell5.innerHTML = list_data[i].type;
        cell6 = row.insertCell(5);
        cell6.innerHTML = list_data[i].value;
    }
}

let map = L.map('mapid').setView([51.231232, 4.440992], 15);
let marker = null;

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([51.231232, 4.440992]).addTo(map)
    .bindPopup('Antwerpen<br>Sportpaleis')
    .openPopup();

map.on('click', function(e){
    if (marker !== null) {
        map.removeLayer(marker);
    }
    addMarker(e)
});

function addMarker(e){
    let radius = document.getElementById("distance").value;
    coordX = e.latlng.lat;
    coordY = e.latlng.lng;
// Add marker to map at click location; add popup window
    marker = L.circle(e.latlng, {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: radius
    }).addTo(map);}