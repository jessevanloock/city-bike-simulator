let url = '';
let linegraph_data = new Map();
let coordX = 0;
let coordY = 0;

function filterData(){
    url = 'http://localhost:8082/api/linegraph_data';
    let typeMeting = document.querySelector('input[name="type"]:checked').value;
    let startDate = document.querySelector('input[name="startdate"]').value;
    let endDate = document.querySelector('input[name="enddate"]').value;
    let distance = document.querySelector('input[name="distance"]').value;
    url += "?type="+typeMeting+"&startdate="+startDate+"&enddate="+endDate+"&distance="+distance+"&coordX="+coordX+"&coordY="+coordY;
    fetchData()
    return false;
}

async function fetchData() {
    const response = await fetch(url);
    const myJson = await response.json();
    console.log(JSON.stringify(myJson));

    linegraph_data = new Map(Object.entries(myJson));

    let values = [];
    let dates = [];
    linegraph_data.forEach((value, key) => values.push(value) && dates.push(key));

    let ctx = document.getElementById('lineGraph').getContext('2d');
    let chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dates,
            datasets: [{
                label: 'My First dataset',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: values
            }]
        },
        options: {}
    });
}

let map = L.map('mapid').setView([51.231232, 4.440992], 15);
let marker = null;

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([51.231232, 4.440992]).addTo(map)
    .bindPopup('Antwerpen<br>Sportpaleis')
    .openPopup();

map.on('click', function(e){
    if (marker !== null) {
        map.removeLayer(marker);
    }
    addMarker(e)
});

function addMarker(e){
    let radius = document.getElementById("distance").value;
    coordX = e.latlng.lat;
    coordY = e.latlng.lng;
// Add marker to map at click location; add popup window
    marker = L.circle(e.latlng, {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: radius
    }).addTo(map);}

filterData();