let url = '';
let heatmap_data = [];

filterData();

let map = new L.Map('map').setView([51.231232, 4.440992], 12);

function filterData(){
    url = 'http://localhost:8082/api/heatmap_data';
    let typeMeting = document.querySelector('input[name="type"]:checked').value;
    let startDate = document.querySelector('input[name="startdate"]').value;
    let endDate = document.querySelector('input[name="enddate"]').value;
    url += "?type="+typeMeting+"&startdate="+startDate+"&enddate="+endDate;
    fetchData();
    return false;
}

async function fetchData() {
    const response = await fetch(url);
    const myJson = await response.json();
    console.log(JSON.stringify(myJson));

    heatmap_data = JSON.parse(JSON.stringify(myJson));

    let min = 0;
    let max = 5;
    let data = heatmap_data.map(function (p) {
        let intensity = parseFloat(p[2]) || 0;
        let normalized = (intensity - min) / (max - min);
        return {lat: p[0], lng: p[1], count: parseFloat(p[2]) || 0};
    });
    console.log("DATA", data);

    let testData = {
        max: max,
        data: data
    };

    let cfg = {
        "radius": 15,
        "maxOpacity": .5,
        "scaleRadius": false,
        "useLocalExtrema": true,
        latField: 'lat',
        lngField: 'lng',
        valueField: 'count',
        blur: 0.95,
        gradient: {
            '.5': 'yellow',
            '.8': 'orange',
            '.95': 'red'
        }
    };

    map.eachLayer(function (layer) {
        map.removeLayer(layer);
    });

    L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
        {maxZoom: 19}).addTo(map);

    let heatmapLayer = new HeatmapOverlay(cfg).addTo(map);

    heatmapLayer.setData(testData);
}