package be.kdg.sa.sensorservice.repository;

import be.kdg.sa.sensorservice.model.SensorMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

/**
 * SensorMessageRepository is a JpaRepository interface used for receiving and saving SensorMessages from/into the database.
 *
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 12-10-2019
 */
public interface SensorMessageRepository extends JpaRepository<SensorMessage, Long> {

    @Query(value = "SELECT * FROM \"SENSOR_MESSAGE\" WHERE \"TYPE\" = :type AND \"TIME_STAMP\" >= :start AND \"TIME_STAMP\" <= :end ", nativeQuery = true)
    List<SensorMessage> findByFilter(String type, String start, String end);

}
