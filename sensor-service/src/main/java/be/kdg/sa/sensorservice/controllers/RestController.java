package be.kdg.sa.sensorservice.controllers;

import be.kdg.sa.sensorservice.model.SensorMessage;
import be.kdg.sa.sensorservice.services.SensorService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.time.LocalDateTime;
import java.util.*;

/**
 * RestController is a class used by the list, linegraph and heatmap javascript scripts which calls this controller and uses its data
 * to populate the list, linegraph and heatmap.
 *
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 28-10-2019
 */
@org.springframework.web.bind.annotation.RestController
public class RestController {

    private final SensorService sensorService;

    /**
     * Constructor for class RestController
     *
     * @param sensorService SensorService class used to access the repository for retrieving data
     */
    public RestController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    /**
     * Creates a new hashmap and puts the timestamp of the filtered sensorMessages as a key in the hashmap
     * and their values as values in the hashmap.
     *
     * @param type the type of the measured value
     * @param startDate the start date on which the sensorMessages get filtered
     * @param endDate the end date on which the sensorMessages get filtered
     */
    @RequestMapping("/api/linegraph_data")
    public Map<LocalDateTime,Float> linegraphData(@RequestParam(value="type", defaultValue="CO2") String type, @RequestParam("startdate") String startDate, @RequestParam("enddate") String endDate, @RequestParam("distance") long distance, @RequestParam("coordX") double coordX, @RequestParam("coordY") double coordY){
        Map<LocalDateTime,Float> data = new HashMap<>();
        List<SensorMessage> sensorMessages = sensorService.filter(type,startDate,endDate,distance,coordX,coordY);
        sensorMessages.forEach(sensorMessage ->
                data.put(sensorMessage.getTimeStamp(),sensorMessage.getValue()));
        return new TreeMap<>(data);
    }

    /**
     * Returns a list of the filtered sensorMessages based on the filter parameters
     *
     * @param type the type of the measured value
     * @param startDate the start date on which the sensorMessages get filtered
     * @param endDate the end date on which the sensorMessages get filtered
     */
    @RequestMapping("/api/list_data")
    public List<SensorMessage> listData(@RequestParam(value="type", defaultValue="CO2") String type, @RequestParam("startdate") String startDate, @RequestParam("enddate") String endDate, @RequestParam("distance") long distance, @RequestParam("coordX") double coordX, @RequestParam("coordY") double coordY){
        return sensorService.filter(type,startDate,endDate, distance, coordX, coordY);
    }

    /**
     * Creates a new linkedlist and puts a list of doubles with the coordinates and values of the filtered sensorMessages in the list.
     *
     * @param type the type of the measured value
     * @param startDate the start date on which the sensorMessages get filtered
     * @param endDate the end date on which the sensorMessages get filtered
     */
    @RequestMapping("/api/heatmap_data")
    public List<List<Double>> heatmapData(@RequestParam(value="type", defaultValue="CO2") String type, @RequestParam("startdate") String startDate, @RequestParam("enddate") String endDate){
        List<SensorMessage> sensorMessages = sensorService.filter(type,startDate,endDate,0,0,0);
        List<List<Double>> addressPoints = new LinkedList<>();
        sensorMessages.forEach(sensorMessage -> addressPoints.add(new LinkedList<Double>(Arrays.asList(sensorMessage.getxCoord(), sensorMessage.getyCoord(), (double)sensorMessage.getValue()))));
        return addressPoints;
    }
}
