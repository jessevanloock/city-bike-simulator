package be.kdg.sa.sensorservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SensorserviceApplication is our main class where we run the spring application.
 *
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 28-09-2019
 */
@SpringBootApplication
public class SensorserviceApplication{

	public SensorserviceApplication(){}

	public static void main(String[] args) {
		SpringApplication.run(SensorserviceApplication.class, args);
	}
}
