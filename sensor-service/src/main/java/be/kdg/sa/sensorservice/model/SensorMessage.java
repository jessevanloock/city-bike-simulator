package be.kdg.sa.sensorservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * SensorMessage is a class used for creating instances of a SensorMessage and saving the messages in the database
 *
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 08-10-2019
 */
@Entity
public class SensorMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id;

    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime timeStamp;

    @Column(nullable = false)
    private double xCoord;

    @Column(nullable = false)
    private double yCoord;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    private float value;

        public SensorMessage(LocalDateTime timeStamp, double xCoord, double yCoord, String type, float value) {
            this.timeStamp = timeStamp;
            this.xCoord = xCoord;
            this.yCoord = yCoord;
            this.type = type;
            this.value = value;
        }

        public SensorMessage(){

        }

    public void setId(long id) {
        this.id = id;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setxCoord(double xCoord) {
        this.xCoord = xCoord;
    }

    public void setyCoord(double yCoord) {
        this.yCoord = yCoord;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public long getId() {
        return id;
    }

        public LocalDateTime getTimeStamp() {
            return timeStamp;
        }

        public double getxCoord() {
            return xCoord;
        }

        public double getyCoord() {
            return yCoord;
        }

        public String getType() {
            return type;
        }

        public float getValue() {
            return value;
        }
    }
