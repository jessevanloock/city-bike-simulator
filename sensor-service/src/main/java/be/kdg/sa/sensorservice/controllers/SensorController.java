package be.kdg.sa.sensorservice.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * SensorController is a class used for displaying the views.
 *
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 08-10-2019
 */
@Controller
public class SensorController {

    @GetMapping("/list")
    public String list() {
    return "list";
    }

    @GetMapping("/linegraph")
    public String linegraph() {
        return "linegraph";
    }

    @GetMapping("/heatmap")
    public String heatmap() { return "heatmap"; }
}
