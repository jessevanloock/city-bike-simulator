package be.kdg.sa.sensorservice.services;

import be.kdg.sa.sensorservice.model.SensorMessage;
import be.kdg.sa.sensorservice.repository.SensorMessageRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SensorService is a service class used for accessing the repository and saving the messages from the queue in the database.
 *
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 12-10-2019
 */
@Service
@Transactional
public class SensorService {
    private final SensorMessageRepository repository;
    private final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorService.class);


    public SensorService(SensorMessageRepository repository) {
        this.repository = repository;
    }

    public List<SensorMessage> filter(String type, String startDate, String endDate, long distance, double coordX, double coordY){
            List<SensorMessage> allSensorMessages = repository.findByFilter(type, startDate, endDate);
            if (coordX < 0.1){
                return allSensorMessages;
            }
        return allSensorMessages.stream().filter(sensorMessage -> distFrom(coordX,coordY,sensorMessage.getxCoord(),sensorMessage.getyCoord()) <= distance).collect(Collectors.toList());
    }

    @RabbitListener(queues = "sensorQueue")
    public void listen(String in){
        SensorMessage message = null;
        try {
           message = gson.fromJson(in, SensorMessage.class);
        }catch (Exception e){
            LOGGER.error("Something went wrong with converting json to sensormessage: " + e.getMessage());
        }
        try {
            repository.saveAndFlush(message);
        }catch (Exception e){
            LOGGER.error("Something went wrong with saving message to database: " + e.getMessage());
        }
    }

    public static float distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return (float) (earthRadius * c);
    }
}
