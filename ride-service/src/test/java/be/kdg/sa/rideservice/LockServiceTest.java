package be.kdg.sa.rideservice;

import be.kdg.sa.rideservice.domain.Lock;
import be.kdg.sa.rideservice.domain.Station;
import be.kdg.sa.rideservice.exceptions.ResourceNotFoundException;
import be.kdg.sa.rideservice.service.LockService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LockServiceTest {

    @Autowired
    private LockService lockService;

    @Test
    public void getAllByStation_stationsShouldBeSame() throws ResourceNotFoundException {
        // TODO: Test on Station without having to use the StationService
        List<Lock> locks = lockService.getAllByStation(1);
        Station station = locks.get(0).getStation();
        if (!locks.isEmpty()){
            locks.forEach(l -> Assert.assertSame(
                    l.getStation(),
                    station
            ));
        } else {
            Assert.fail("No locks found to test, try a different Station");
        }
    }

    @Test
    public void getAllByStation_shouldThrowExceptionIfStationDoesNotExist() {
        try {
            List<Lock> locks = lockService.getAllByStation(0);
            Assert.fail("No ResourceNotFoundException was thrown");
        } catch (Exception e) {
            Assert.assertSame(e.getClass(), ResourceNotFoundException.class);
        }
    }

    @Test
    public void getAllByStationWithVehicle_vehicleShouldNotBeNull() throws ResourceNotFoundException {
        List<Lock> locks = lockService.getAllByStationWithVehicle(2);
        if (!locks.isEmpty()){
            locks.forEach(l -> Assert.assertNotNull(l.getVehicle()));
        } else {
            Assert.fail("No locks found to test, try a different Station");
        }
    }

    @Test
    public void getAllByStationWithoutVehicle_vehicleShouldBeNull() throws ResourceNotFoundException {
        List<Lock> locks = lockService.getAllByStationWithoutVehicle(1);
        if (!locks.isEmpty()){
            locks.forEach(l -> Assert.assertNull(l.getVehicle()));
        } else {
            Assert.fail("No locks found to test, try a different Station");
        }
    }
}
