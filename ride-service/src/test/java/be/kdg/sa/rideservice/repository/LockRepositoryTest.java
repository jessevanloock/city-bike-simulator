package be.kdg.sa.rideservice.repository;

import be.kdg.sa.rideservice.dao.lock.LockRepository;
import be.kdg.sa.rideservice.domain.Lock;
import be.kdg.sa.rideservice.domain.Station;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LockRepositoryTest {

    @Autowired
    private LockRepository lockRepository;

    @Test
    public void findById() {
        Optional<Lock> lock = lockRepository.findById(1);

        lock.ifPresent(l ->
                Assert.assertSame(
                        l.getClass(),
                        Lock.class));

        lock.ifPresent(l ->
                Assert.assertEquals(
                        l.getId(),
                        1));
    }

    @Test
    public void findAllByStation_shouldHaveSameStations() {
        Station station = new Station();
        station.setId(1);
        List<Lock> locks = lockRepository.findAllByStation(station);

        if (!locks.isEmpty()) {
            Station stationToTest = locks.get(0).getStation();
            locks.forEach(l ->
                    Assert.assertSame(
                            l.getStation(),
                            stationToTest));
        } else Assert.fail("No Locks found");
    }

    @Test
    public void findAllByStationAndVehicleIsNull_shouldHaveSameStationsAndVehicleShouldBeNull() {
        Station station = new Station();
        station.setId(1);
        List<Lock> locks = lockRepository.findAllByStationAndVehicleIsNull(station);

        if (!locks.isEmpty()) {
            Station stationToTest = locks.get(0).getStation();
            locks.forEach(l ->
                    Assert.assertSame(
                            l.getStation(),
                            stationToTest));
            locks.forEach(l ->
                    Assert.assertNull(l.getVehicle()));
        } else Assert.fail("No Locks found");
    }

    @Test
    public void findAllByStationAndVehicleIsNotNull_shouldHaveSameStationsAndVehicleShouldBeNotNull() {
        Station station = new Station();
        station.setId(2);
        List<Lock> locks = lockRepository.findAllByStationAndVehicleIsNotNull(station);

        if (!locks.isEmpty()) {
            Station stationToTest = locks.get(0).getStation();
            locks.forEach(l ->
                    Assert.assertSame(
                            l.getStation(),
                            stationToTest));
            locks.forEach(l ->
                    Assert.assertNotNull(l.getVehicle()));
        } else Assert.fail("No Locks found");
    }
}
