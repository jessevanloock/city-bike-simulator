package be.kdg.sa.rideservice.repository;

import be.kdg.sa.rideservice.dao.vehicle.VehicleRepository;
import be.kdg.sa.rideservice.domain.Vehicle;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VehicleRepositoryTest {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Test
    public void findById() {
        Optional<Vehicle> vehicle = vehicleRepository.findById(1);

        vehicle.ifPresent(v ->
                Assert.assertSame(
                        v.getClass(),
                        Vehicle.class));

        vehicle.ifPresent(v ->
                Assert.assertEquals(
                        v.getId(),
                        1));
    }

    @Test
    public void findAllByBikelotBikeTypeDescription() {
        List<Vehicle> vehicles = vehicleRepository.findAllByBikelot_BikeType_Description("Velo Bike");

        if (!vehicles.isEmpty()) vehicles.forEach(v ->
                Assert.assertEquals(
                        v.getBikelot().getBikeType().getDescription(),
                        "Velo Bike"));
        else Assert.fail("No vehicles found");
    }
}
