package be.kdg.sa.rideservice.repository;

import be.kdg.sa.rideservice.dao.ride.RideRepository;
import be.kdg.sa.rideservice.domain.Ride;
import be.kdg.sa.rideservice.domain.Subscription;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RideRepositoryTest {

    @Autowired
    private RideRepository rideRepository;

    @Test
    public void findById() {
        Optional<Ride> ride = rideRepository.findById(1663540);

        ride.ifPresent(r ->
                Assert.assertSame(
                        r.getClass(),
                        Ride.class));

        ride.ifPresent(r ->
                Assert.assertEquals(
                        r.getId(),
                        1663540));
    }

    @Test
    public void findAllByEndTimeIsNullAndStartTimeIsBefore() {
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        List<Ride> rides = rideRepository.findAllByEndTimeIsNullAndStartTimeIsBefore(
                timestamp);

        for (Ride ride: rides) {
            Assert.assertNull(ride.getEndTime());
            Assert.assertTrue(ride.getStartTime().before(timestamp));
        }
    }

    @Test
    public void findAllBySubscription() {
        Subscription subscription = new Subscription();
        subscription.setId(1);
        List<Ride> rides = rideRepository.findAllBySubscription(subscription);

        rides.forEach(r ->
                Assert.assertEquals(
                        r.getSubscription().getId(),
                        subscription.getId()));
    }

    @Test
    public void findAllBySubscriptionAndEndTimeIsNull() {
        Subscription subscription = new Subscription();
        subscription.setId(1);
        List<Ride> rides = rideRepository.findAllBySubscriptionAndEndTimeIsNull(subscription);

        for (Ride ride: rides) {
            Assert.assertEquals
                    (ride.getSubscription().getId(),
                            subscription.getId());
            Assert.assertNull(ride.getEndTime());
        }
    }
}
