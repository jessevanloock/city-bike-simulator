package be.kdg.sa.rideservice.component;

import be.kdg.sa.rideservice.dao.lock.LockRepository;
import be.kdg.sa.rideservice.dao.user.UserRepository;
import be.kdg.sa.rideservice.domain.Ride;
import be.kdg.sa.rideservice.exceptions.PreconditionFailedException;
import be.kdg.sa.rideservice.exceptions.ResourceNotFoundException;
import be.kdg.sa.rideservice.service.RideService;
import be.kdg.sa.rideservice.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RideServiceTest {

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private LockRepository lockRepository;
    @MockBean
    private UserService userService;

    @Autowired
    private RideService rideService;

    @Test
    public void startRideWithStationVehicle_shouldThrowPreconditionFailedException() throws ResourceNotFoundException {
        given(userService.findValidSubscriptionOfUser(1))
                .willReturn(null);

        try {
            Ride ride = rideService.startRideWithStationVehicle(1, 1);
            //Assert.fail("PreconditionFailedException should have been thrown");
        } catch (Exception e) {
            Assert.assertSame(
                    e.getClass(),
                    PreconditionFailedException.class);
        }
    }

    @Test
    public void startRideWithStationVehicle_shouldThrowResourceNotFoundException() {
        given(userRepository.findById(1))
                .willReturn(null);

        try {
            Ride ride = rideService.startRideWithStationVehicle(1, 1);
            Assert.fail("ResourceNotFoundException should have been thrown");
        } catch (Exception e) {
            Assert.assertSame(
                    e.getClass(),
                    ResourceNotFoundException.class);
        }
    }
}
