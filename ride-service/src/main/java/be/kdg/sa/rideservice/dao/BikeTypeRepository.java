package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.BikeType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BikeTypeRepository extends JpaRepository<BikeType, Integer> {
}
