package be.kdg.sa.rideservice.service;

import be.kdg.sa.rideservice.dao.SubscriptionRepository;
import be.kdg.sa.rideservice.dao.SubscriptionTypeRepository;
import be.kdg.sa.rideservice.domain.Subscription;
import be.kdg.sa.rideservice.domain.SubscriptionType;
import be.kdg.sa.rideservice.service.generic.IService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * SubscriptionService is a class used for all logic
 * required to use the class SubscriptionRepository and SubscriptionTypeRepository directly
 * and concerning Subscriptions and SubscriptionTypes
 *
 * @author Jonas Marien
 * @version 1.0
 * @since 01-10-2019
 */
@Service
public class SubscriptionService implements IService<Subscription> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleService.class);

    private final SubscriptionRepository subscriptionRepository;
    private final SubscriptionTypeRepository subscriptionTypeRepository;

    /**
     * Constructor for class SubscriptionService
     *
     * @param subscriptionRepository SubscriptionService class used to access the dao layer of this application
     * @param subscriptionTypeRepository SubscriptionService class used to access the dao layer of this application
     */
    @Autowired
    public SubscriptionService(SubscriptionRepository subscriptionRepository,
                               SubscriptionTypeRepository subscriptionTypeRepository) {
        this.subscriptionRepository = subscriptionRepository;
        this.subscriptionTypeRepository = subscriptionTypeRepository;
    }


    //<editor-fold desc="CRUD">
    //<editor-fold desc="Subscription">

    /**
     * Retrieves a Subscription using an id
     *
     * @param id Id of the Subscription
     * @return Subscription object
     */
    @Override
    public Subscription get(int id) {
        return subscriptionRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing Subscription
     *
     * @param entity Subscription to create or update
     * @return Created or updated Subscription object
     */
    @Override
    public Subscription save(Subscription entity) {
        return subscriptionRepository.save(entity);
    }

    /**
     * Deletes a Subscription
     *
     * @param entity Subscription to delete
     */
    @Override
    public void delete(Subscription entity) {
        subscriptionRepository.delete(entity);
    }

    /**
     * Deletes a Subscription using an Id
     *
     * @param id Id of a Subscription
     */
    @Override
    public void deleteById(int id) {
        subscriptionRepository.deleteById(id);
    }

    /**
     * Retrieves all Subscriptions
     *
     * @return List of Subscription objects
     */
    @Override
    public List<Subscription> getAll() {
        return subscriptionRepository.findAll();
    }
    //</editor-fold>
    //<editor-fold desc="SubscriptionType">

    /**
     * Retrieves a SubscriptionType using an id
     *
     * @param id Id of the SubscriptionType
     * @return SubscriptionType object
     */
    public SubscriptionType getType(int id){
        return subscriptionTypeRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existsing SubscriptionType
     *
     * @param entity SubscriptionType to create or update
     * @return Created or updated SubscriptionType object
     */
    public SubscriptionType saveType(SubscriptionType entity){
        return subscriptionTypeRepository.save(entity);
    }

    /**
     * Deletes the SubscriptionType
     *
     * @param entity SubscriptionType to delete
     */
    public void deleteType(SubscriptionType entity){
        subscriptionTypeRepository.delete(entity);
    }

    /**
     * Deletes a SubscriptionType using an Id
     *
     * @param id Id of a SubscriptionType
     */
    public void deleteTypeById(int id){
        subscriptionTypeRepository.deleteById(id);
    }

    /**
     * Retrieves all SubscriptionTypes
     *
     * @return List of SubscriptionType objects
     */
    public List<SubscriptionType> getAllTypes(){
        return subscriptionTypeRepository.findAll();
    }
    //</editor-fold>
    //</editor-fold>
}

