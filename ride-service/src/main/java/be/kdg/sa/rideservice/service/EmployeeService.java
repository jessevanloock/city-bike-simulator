package be.kdg.sa.rideservice.service;

import be.kdg.sa.rideservice.dao.EmployeeRepository;
import be.kdg.sa.rideservice.domain.Employee;
import be.kdg.sa.rideservice.service.generic.IService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * EmployeeService is a class used for all logic
 * required to use the class EmployeeRepository directly
 * and concerning Employees
 *
 * @author Jonas Marien
 * @version 1.0
 * @since 01-10-2019
 */
@Service
public class EmployeeService implements IService<Employee> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleService.class);

    private final EmployeeRepository employeeRepository;

    /**
     * Constructor for class EmployeeService
     *
     * @param employeeRepository EmployeeRepository class used to access the dao layer of this application
     */
    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    //<editor-fold desc="CRUD">
    /**
     * Retrieves an Employee using an id
     *
     * @param id Id of the Employee
     * @return Employee object
     */
    @Override
    public Employee get(int id) {
        return employeeRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing Employee
     *
     * @param entity Employee to create or update
     * @return Created or updated Employee object
     */
    @Override
    public Employee save(Employee entity) {
        return employeeRepository.save(entity);
    }

    /**
     * Deletes the Employee
     *
     * @param entity Employee to delete
     */
    @Override
    public void delete(Employee entity) {
        employeeRepository.delete(entity);
    }

    /**
     * Deletes an Employee using an Id
     *
     * @param id Id of an Employee
     */
    @Override
    public void deleteById(int id) {
        employeeRepository.deleteById(id);
    }

    /**
     * Retrieves all Employees
     *
     * @return List of Employee  objects
     */
    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }
    //</editor-fold>
}
