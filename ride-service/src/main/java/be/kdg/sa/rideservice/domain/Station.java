package be.kdg.sa.rideservice.domain;

import com.vividsolutions.jts.geom.Point;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Stations")
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "StationId", columnDefinition = "SMALLINT")
    private int id;

    @Column(name = "ObjectId", columnDefinition = "NVARCHAR", length = 20, nullable = false)
    private String objectId;

    @Column(name = "StationNr", columnDefinition = "NVARCHAR", length = 20, nullable = false)
    private String stationNr;

    @Column(name = "Type", columnDefinition = "NVARCHAR", length = 20, nullable = false)
    private String type;

    @Column(name = "Street",columnDefinition = "NVARCHAR", length = 100, nullable = false)
    private String street;

    @Column(name = "Number",columnDefinition = "NVARCHAR", length = 10, nullable = false)
    private String number;

    @Column(name = "Zipcode",columnDefinition = "NVARCHAR", length = 10, nullable = false)
    private String zipcode;

    @Column(name = "District", columnDefinition = "NVARCHAR", length = 100, nullable = false)
    private String district;

<<<<<<< HEAD
    @Column(name = "GPSCoord", nullable = false)
    private Point gpsCoord;
=======
    //@Column(name = "GPSCoord", nullable = false)
    //private Point gpsCoord;
>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af

    @Column(name = "AdditionalInfo", columnDefinition = "NVARCHAR", length = 100)
    private String additionalInfo;

    @Column(name = "LabelId", columnDefinition = "TINYINT")
    private Integer labelId; //int is not nullable, only null values in our database so using Integer as a work-around

    @Column(name = "CityId", columnDefinition = "TINYINT")
    private Integer cityId; //int is not nullable, only null values in our database so using Integer as a work-around

    @OneToMany(mappedBy = "station",cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Lock> locks;

    public Station() {
        this.objectId = "00000";
        this.stationNr = "00000";
        this.type = "type";
        this.street = "street";
        this.number = "number";
        this.zipcode = "00000";
        this.district = "district";
        // this.gpsCoord = ?? new Point(CoordinateSequence interface, blabla)
    }

    public Station(String objectId, String stationNr, String type, String street, String number, String zipcode, String district, Point gpsCoord) {
        this.objectId = objectId;
        this.stationNr = stationNr;
        this.type = type;
        this.street = street;
        this.number = number;
        this.zipcode = zipcode;
        this.district = district;
        //this.gpsCoord = gpsCoord;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getStationNr() {
        return stationNr;
    }

    public void setStationNr(String stationNr) {
        this.stationNr = stationNr;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    /*public Point getGpsCoord() {
        return gpsCoord;
    }

    public void setGpsCoord(Point gpsCoord) {
        this.gpsCoord = gpsCoord;
    }*/

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Integer getLabelId() {
        return labelId;
    }

    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public List<Lock> getLocks() {
        return locks;
    }

    public void setLocks(List<Lock> locks) {
        this.locks = locks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getStationNr() {
        return stationNr;
    }

    public void setStationNr(String stationNr) {
        this.stationNr = stationNr;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Point getGpsCoord() {
        return gpsCoord;
    }

    public void setGpsCoord(Point gpsCoord) {
        this.gpsCoord = gpsCoord;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Integer getLabelId() {
        return labelId;
    }

    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public List<Lock> getLocks() {
        return locks;
    }

    public void setLocks(List<Lock> locks) {
        this.locks = locks;
    }
}
