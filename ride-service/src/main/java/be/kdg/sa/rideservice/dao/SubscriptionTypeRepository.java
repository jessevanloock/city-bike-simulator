package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.SubscriptionType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionTypeRepository extends JpaRepository<SubscriptionType, Integer> {
}
