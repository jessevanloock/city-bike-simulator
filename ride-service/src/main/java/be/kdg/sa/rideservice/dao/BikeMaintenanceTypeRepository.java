package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.BikeMaintenanceType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BikeMaintenanceTypeRepository extends JpaRepository<BikeMaintenanceType, Integer> {
}
