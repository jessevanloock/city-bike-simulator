package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.Ride;
import be.kdg.sa.rideservice.domain.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.List;

public interface RideRepository extends JpaRepository<Ride, Integer> {

    List<Ride> findAllByEndTimeIsNullAndStartTimeIsBefore(Timestamp timestamp);
    List<Ride> findAllBySubscription(Subscription subscription);
    List<Ride> findAllBySubscriptionAndEndTimeIsNull(Subscription subscription);

}
