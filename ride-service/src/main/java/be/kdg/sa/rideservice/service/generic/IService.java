package be.kdg.sa.rideservice.service.generic;

import java.util.List;

public interface IService<T> {

    T get(int id);
    List<T> getAll();
    T save(T entity);
    void delete(T entity);
    void deleteById(int id);
}
