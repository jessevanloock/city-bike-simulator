package be.kdg.sa.rideservice.controller;

import be.kdg.sa.rideservice.controller.dto.FreeVehicleDto;
import be.kdg.sa.rideservice.controller.dto.StationVehicleDto;
import be.kdg.sa.rideservice.domain.Lock;
import be.kdg.sa.rideservice.domain.Ride;
import be.kdg.sa.rideservice.domain.Vehicle;
import be.kdg.sa.rideservice.exceptions.PreconditionFailedException;
import be.kdg.sa.rideservice.exceptions.ResourceNotFoundException;
import be.kdg.sa.rideservice.service.LockService;
import be.kdg.sa.rideservice.service.RideService;
import be.kdg.sa.rideservice.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rideservice")
public class RideServiceRestController {
    private final RideService rideService;
    private final LockService lockService;
    private final VehicleService vehicleService;

    @Autowired
    public RideServiceRestController(RideService rideService,
                                     LockService lockService,
                                     VehicleService vehicleService) {
        this.rideService = rideService;
        this.lockService = lockService;
        this.vehicleService = vehicleService;
    }

    @GetMapping("/getlocks/{stationId}")
    ResponseEntity<List<Integer>> getFreeLocksByStationId(@PathVariable int stationId) {
        try {
            List<Integer> lockIds = lockService.getAllByStationWithoutVehicle(stationId)
                    .stream()
                    .map(Lock::getId)
                    .collect(Collectors.toList());
            return new ResponseEntity<>(lockIds, HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @PostMapping(value = "/unlock-station-vehicle", produces = "application/json; charset=utf-8")
    ResponseEntity<Integer> unlockStationVehicle(@RequestBody StationVehicleDto stationVehicleDto) {
        try {
            Ride ride = rideService.startRideWithStationVehicle(
                    stationVehicleDto.getUserId(),
                    stationVehicleDto.getStationId()
            );
            return new ResponseEntity<>(ride.getStartLock().getStationLockNr(), HttpStatus.ACCEPTED);
        } catch (ResourceNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        } catch (PreconditionFailedException e) {
            throw new ResponseStatusException(
                    HttpStatus.PRECONDITION_FAILED, e.getMessage(), e);
        }
    }

    @PutMapping(value = "/lock-station-vehicle", produces = "application/json; charset=utf-8")
    HttpStatus lockStationVehicle(@RequestBody StationVehicleDto stationVehicleDto) {
        try {
            rideService.endRideWithStationVehicle(
                    stationVehicleDto.getUserId(),
                    stationVehicleDto.getLockId()
            );
            return HttpStatus.ACCEPTED;
        } catch (ResourceNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        } catch (PreconditionFailedException e) {
            throw new ResponseStatusException(
                    HttpStatus.PRECONDITION_FAILED, e.getMessage(), e);
        }
    }

    @PostMapping(value = "/unlock-free-vehicle", produces = "application/json; charset=utf-8")
    ResponseEntity<Boolean> unlockFreeVehicle(@RequestBody FreeVehicleDto freeVehicleDto) {
        try {
            rideService.startRideWithFreeVehicle(
                    freeVehicleDto.getUserId(),
                    freeVehicleDto.getVehicleId()
            );
            return new ResponseEntity<>(true, HttpStatus.CREATED);
        } catch (ResourceNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        } catch (PreconditionFailedException e) {
            throw new ResponseStatusException(
                    HttpStatus.PRECONDITION_FAILED, e.getMessage(), e);
        }
    }

    @PostMapping(value = "/lock-free-vehicle", produces = "application/json; charset=utf-8")
    ResponseEntity<Boolean> lockFreeVehicle(@RequestBody FreeVehicleDto freeVehicleDto) {
        System.out.println("called!");
        try {
            rideService.endRideWithFreeVehicle(
                    freeVehicleDto.getUserId(),
                    freeVehicleDto.getVehicleId()
            );
            System.out.println("Gelukt");
            return new ResponseEntity<>(true, HttpStatus.ACCEPTED);
        } catch (ResourceNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e);
        } catch (PreconditionFailedException e) {
            throw new ResponseStatusException(
                    HttpStatus.PRECONDITION_FAILED, e.getMessage(), e);
        }
    }

    @GetMapping(value = "nearest-free-vehicle", produces = "application/json; charset=utf-8")
    ResponseEntity<FreeVehicleDto> findNearestFreeVehicle(@RequestBody FreeVehicleDto freeVehicleDto) {
        Vehicle vehicle = vehicleService.getNearestVehicleOfType(
                freeVehicleDto.getVehicleType(),
                freeVehicleDto.getxCoord(),
                freeVehicleDto.getyCoord()
        );
        /*return new ResponseEntity<>(
                new FreeVehicleDto(
                        vehicle.getId(),
                        vehicle.getPoint().getX(),
                        vehicle.getPoint().getY()
                ), HttpStatus.OK
        ); // TODO: check if null*/
        return null;
    }
}

