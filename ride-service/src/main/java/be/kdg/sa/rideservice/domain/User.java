package be.kdg.sa.rideservice.domain;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UserId")
    private int id;

    @Column(name = "Name",columnDefinition = "nvarchar", nullable = false)
    private String name;

    @Column(name = "Email",columnDefinition = "nvarchar", nullable = false)
    private String email;

    @Column(name = "Street",columnDefinition = "nvarchar")
    private String street;

    @Column(name = "Number",columnDefinition = "nvarchar")
    private String number;

    @Column(name = "Zipcode",columnDefinition = "nvarchar")
    private String zipcode;

    @Column(name = "City",columnDefinition = "nvarchar")
    private String city;

    @Column(name = "CountryCode",columnDefinition = "nvarchar")
    private String countryCode;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Subscription> subscriptions;

    public User() {
        name = "unavailable";
        email = "unavailable";
    }

    public User(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }
}
