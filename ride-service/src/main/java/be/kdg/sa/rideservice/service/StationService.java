package be.kdg.sa.rideservice.service;

import be.kdg.sa.rideservice.dao.StationRepository;
import be.kdg.sa.rideservice.domain.Station;
import be.kdg.sa.rideservice.service.generic.IService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * StationService is a class used for all logic
 * required to use the class StationRepository directly
 * and concerning Stations
 *
 * @author Jonas Marien
 * @version 1.0
 * @since 01-10-2019
 */
@Service
public class StationService implements IService<Station> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleService.class);

    private final StationRepository stationRepository;

    /**
     * Constructor for class StationService
     *
     * @param stationRepository StationRepository class used to access the dao layer of this application
     */
    @Autowired
    public StationService(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    //<editor-fold desc="CRUD">

    /**
     * Retrieves a Station using an id
     *
     * @param id Id of the Station
     * @return Station object
     */
    @Override
    public Station get(int id) {
        return stationRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing Station
     *
     * @param entity Station to create or update
     * @return Created or updated Station object
     */
    @Override
    public Station save(Station entity) {
        return stationRepository.save(entity);
    }

    /**
     * Deletes the Station
     *
     * @param entity Station to delete
     */
    @Override
    public void delete(Station entity) {
        stationRepository.delete(entity);
    }

    /**
     * Deletes a Station using an Id
     *
     * @param id Id of a Station
     */
    @Override
    public void deleteById(int id) {
        stationRepository.deleteById(id);
    }

    /**
     * Retrieves all Stations
     *
     * @return List of Station objects
     */
    @Override
    public List<Station> getAll() {
        return stationRepository.findAll();
    }
    //</editor-fold>
}

