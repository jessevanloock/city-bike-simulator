package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionRepository extends JpaRepository<Subscription, Integer> {
}
