package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
