package be.kdg.sa.rideservice.service;

import be.kdg.sa.rideservice.dao.BikeMaintenanceTypeRepository;
import be.kdg.sa.rideservice.dao.BikeTypeRepository;
import be.kdg.sa.rideservice.dao.BikelotRepository;
import be.kdg.sa.rideservice.dao.VehicleRepository;
import be.kdg.sa.rideservice.domain.BikeMaintenanceType;
import be.kdg.sa.rideservice.domain.BikeType;
import be.kdg.sa.rideservice.domain.Bikelot;
import be.kdg.sa.rideservice.domain.Vehicle;
import be.kdg.sa.rideservice.exceptions.ResourceNotFoundException;
import be.kdg.sa.rideservice.service.generic.IService;
<<<<<<< HEAD
import be.kdg.sa.rideservice.utility.MyGeometryFactory;
=======
import be.kdg.sa.rideservice.utilities.MyGeometryFactory;
>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * VehicleService is a class used for all logic
 * required to use the class VehicleRepository, BikelotRepository, BikeMaintenanceTypeRepository and BikeTypeRepository directly
 * and concerning Vehicles, Bikelots, BikeMaintenanceTypes and BikeTypes
 *
 * @author Jonas Marien
 * @version 1.0
 * @since 01-10-2019
 */
@Service
public class VehicleService implements IService<Vehicle> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleService.class);

    private final VehicleRepository vehicleRepository;
    private final BikelotRepository bikelotRepository;
    private final BikeMaintenanceTypeRepository bikeMaintenanceTypeRepository;
    private final BikeTypeRepository bikeTypeRepository;
    private final MyGeometryFactory geometryFactory;

    /**
     * Constructor for class VehicleService
     *  @param vehicleRepository VehicleRepository class used to access the dao layer of this application
     * @param bikelotRepository BikelotRepository class used to access the dao layer of this application
     * @param bikeMaintenanceTypeRepository BikeMaintenanceTypeRepository class used to access the dao layer of this application
     * @param bikeTypeRepository BikeTypeRepository class used to access the dao layer of this application
     * @param geometryFactory GeometryFactory class used to create new Point objects
     */
    @Autowired
    public VehicleService(VehicleRepository vehicleRepository,
                          BikelotRepository bikelotRepository,
                          BikeMaintenanceTypeRepository bikeMaintenanceTypeRepository,
                          BikeTypeRepository bikeTypeRepository,
                          MyGeometryFactory geometryFactory) {
        this.vehicleRepository = vehicleRepository;
        this.bikelotRepository = bikelotRepository;
        this.bikeMaintenanceTypeRepository = bikeMaintenanceTypeRepository;
        this.bikeTypeRepository = bikeTypeRepository;
        this.geometryFactory = geometryFactory;
    }

    //<editor-fold desc="CRUD">
    //<editor-fold desc="Vehicle">

    /**
     * Retrieves a Vehicle using an id
     *
     * @param id Id of the Vehicle
     * @return Vehicle object
     */
    @Override
    public Vehicle get(int id) {
        return vehicleRepository.findById(id).orElse(null);
    }

    /**
     * Retrieves all Vehicles
     *
     * @return List of Vehicle objects
     */
    @Override
    public List<Vehicle> getAll() {
        return vehicleRepository.findAll();
    }

    /**
     * Creates a new or updates an existing Vehicle
     *
     * @param entity Vehicle to create or update
     * @return Created or updated Vehicle object
     */
    @Override
    public Vehicle save(Vehicle entity) {
        return vehicleRepository.save(entity);
    }

    /**
     * Deletes the Vehicle
     *
     * @param entity Vehicle to delete
     */
    @Override
    public void delete(Vehicle entity) {
        vehicleRepository.delete(entity);
    }

    /**
     * Deletes the Vehicle using an Id
     *
     * @param id Id of the vehicle
     */
    @Override
    public void deleteById(int id) {
        vehicleRepository.deleteById(id);
    }

    //</editor-fold>
    //<editor-fold desc="Bikelot">

    /**
     * Retrieves a Bikelot using an id
     *
     * @param id Id of the Bikelot
     * @return Bikelot object
     */
    public Bikelot getBikelot(int id){
        return bikelotRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing Bikelot
     *
     * @param entity Bikelot to create or update
     * @return Created or updated Bikelot object
     */
    public Bikelot saveBikelot(Bikelot entity){
        return bikelotRepository.save(entity);
    }

    /**
     * Deletes a Bikelot
     *
     * @param entity Bikelot object to delete
     */
    public void deleteBikelot(Bikelot entity){
        bikelotRepository.delete(entity);
    }

    /**
     * Deletes a Bikelot using an Id
     *
     * @param id Id of the Bikelot
     */
    public void deleteBikeLotById(int id) {
        bikelotRepository.deleteById(id);
    }

    /**
     * Retrieves all Bikelots
     *
     * @return List of Bikelot objects
     */
    public List<Bikelot> getAllBikelots(){
        return bikelotRepository.findAll();
    }
    //</editor-fold>
    //<editor-fold desc="BikeMaintenanceType">

    /**
     * Retrieves a MaintenanceType using an id
     *
     * @param id Id of the MaintenanceType
     * @return MaintenanceType object
     */
    public BikeMaintenanceType getMaintenanceType(int id){
        return bikeMaintenanceTypeRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing MaintenanceType
     *
     * @param entity MaintenanceType object to create or update
     * @return Created or updated MaintenanceType object
     */
    public BikeMaintenanceType saveMaintenanceType(BikeMaintenanceType entity){
        return bikeMaintenanceTypeRepository.save(entity);
    }

    /**
     * Deletes a MaintenanceType
     *
     * @param entity MaintenanceType object to delete
     */
    public void deleteMaintenanceType(BikeMaintenanceType entity){
        bikeMaintenanceTypeRepository.delete(entity);
    }

    /**
     * Deletes a MaintenanceType using an Id
     *
     * @param id Id of a MaintenanceType
     */
    public void deleteMaintenanceTypeById(int id){
        bikeMaintenanceTypeRepository.deleteById(id);
    }

    /**
     * Retrieves all MaintenanceTypes
     *
     * @return List of MaintenanceType objects
     */
    public List<BikeMaintenanceType> getAllMaintenanceTypes(){
        return bikeMaintenanceTypeRepository.findAll();
    }
    //</editor-fold>
    //<editor-fold desc="BikeType">

    /**
     * Retrieves a BikeType using an id
     *
     * @param id Id of the BikeType
     * @return BikeType object
     */
    public BikeType getBikeType(int id){
        return bikeTypeRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing BikeType
     *
     * @param entity BikeType object to create or update
     * @return Created or updated BikeType object
     */
    public BikeType saveBikeType(BikeType entity){
        return bikeTypeRepository.save(entity);
    }

    /**
     * Deletes a BikeType
     *
     * @param entity BikeType object to delete
     */
    public void deleteBikeType(BikeType entity){
        bikeTypeRepository.delete(entity);
    }

    /**
     * Deletes a BikeType using an Id
     *
     * @param id Id of a BikeType
     */
    public void deleteBikeTypeById(int id){
        bikeTypeRepository.deleteById(id);
    }

    /**
     * Retrieves all BikeTypes
     *
     * @return List of BikeType objects
     */
    public List<BikeType> getAllBikeTypes(){
        return bikeTypeRepository.findAll();
    }
    //</editor-fold>
    //</editor-fold>

    /**
     * Retrieves all Vehicles of type "Step" and "Scooter"
     *
     * @return List of Vehicle objects
     */
    public List<Vehicle> getAllFreeVehicles() {
        return vehicleRepository.findAll()
                .stream()
                .filter(v ->
                        v.getBikelot().getBikeType().getDescription().equals("Step")
                                || v.getBikelot().getBikeType().getDescription().equals("Scooter"))
                .collect(Collectors.toList());
    }

    /**
     * Retrieves all Vehicles of type "Velo Bike" and "Velo E-Bike"
     *
     * @return List of Vehicle objects
     */
    public List<Vehicle> getAllStationVehicles() {
        return vehicleRepository.findAll()
                .stream()
                .filter(v ->
                        v.getBikelot().getBikeType().getDescription().equals("Velo Bike")
                                || v.getBikelot().getBikeType().getDescription().equals("Velo E-Bike"))
                .collect(Collectors.toList());
    }

    /**
     * Retrieves the nearest Vehicle of a given BikeType to given X and Y coordinates
     *
     * @param type String based on the description of a BikeType object
     * @param x X coordinate of the given location
     * @param y Y coordinate of the given location
     * @return Vehicle object
     */
    public Vehicle getNearestVehicleOfType(String type, double x, double y) {
        double radius = 100; // TODO: Maybe initialise this dynamically?
        double vX = 0;
        double vY = 0;
        Vehicle nearestVehicle = null;
        double nvX = 0;
        double nvY = 0;

        // TODO: optimize by adding a pointBetween in query (?)
        List<Vehicle> vehiclesOfType = vehicleRepository.findAllByBikelot_BikeType_Description(type);
        /*for (Vehicle vehicle: vehiclesOfType) {
            vX = vehicle.getPoint().getX();
            vY = vehicle.getPoint().getY();

            if (x-radius <= vX && vX <= x+radius
                    && y-radius <= vY && vY <= y+radius){ // Vehicle is in search radius
                if (nearestVehicle != null) {
                    if (Math.abs(vX - x) <= Math.abs(nvX - x) && Math.abs(vY - y) <= Math.abs(nvY - y)) {
                        nearestVehicle = vehicle; // Vehicle is closer to x,y than nearestVehicle
                    }
                } else {
                    nearestVehicle = vehicle; // Initialise vehicle for the first time
                }
            } // Vehicle is not in the search radius
        }*/
        return nearestVehicle;
    }

    /**
     * Updates the location of a Vehicle
     *
     * @param vehicleId Id of the Vehicle
     * @param x X coordinate of the new location
     * @param y Y coordinate of the new location
     * @param timestamp Timestamp of the update
     * @throws ResourceNotFoundException Thrown if Vehicle was not found
     */
    public void updateLocation(int vehicleId, double x, double y, Timestamp timestamp)
            throws ResourceNotFoundException {

        Vehicle vehicle = vehicleRepository.findById(vehicleId)
                .orElseThrow(() -> new ResourceNotFoundException("Vehicle not found"));

        //vehicle.setPoint(geometryFactory.createPoint(new Coordinate(x,y)));
        // TODO: Log the update using the timestamp
        vehicleRepository.save(vehicle);
        LOGGER.info(String.format("[%s] Location updated of vehicle {%d}: (%s,%s)", timestamp, vehicle.getId(), x, y));
    }
}

