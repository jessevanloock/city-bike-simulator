package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {
    List<Vehicle> findAllByBikelot_BikeType_Description(String description);
}
