package be.kdg.sa.rideservice.domain;

import javax.persistence.*;

@Entity(name = "BikeMaintenanceTypes")
public class BikeMaintenanceType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BikeMaintenanceTypeId", columnDefinition = "TINYINT")
    private int id;

    @Column(name = "Description", columnDefinition = "NVARCHAR")
    private String description;

    public BikeMaintenanceType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
