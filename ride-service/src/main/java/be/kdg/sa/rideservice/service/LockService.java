package be.kdg.sa.rideservice.service;

import be.kdg.sa.rideservice.dao.LockRepository;
import be.kdg.sa.rideservice.domain.Lock;
import be.kdg.sa.rideservice.domain.Station;
import be.kdg.sa.rideservice.exceptions.ResourceNotFoundException;
import be.kdg.sa.rideservice.service.generic.IService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * LockService is a class used for all logic
 * required to use the class LockRepository directly
 * and concerning Locks
 *
 * @author Jonas Marien
 * @version 1.0
 * @since 01-10-2019
 */
@Service
public class LockService implements IService<Lock> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleService.class);

    private final LockRepository lockRepository;

    private final StationService stationService;

    /**
     * Constructor for class LockService
     *
     * @param lockRepository LockRepository class used to access the dao layer of this application
     * @param stationService StationRepository class used to access the dao layer of this application
     */
    @Autowired
    public LockService(LockRepository lockRepository,
                       StationService stationService) {
        this.lockRepository = lockRepository;
        this.stationService = stationService;
    }

    //<editor-fold desc="CRUD">

    /**
     * Retrieves a Lock using an id
     *
     * @param id Id of the Lock
     * @return Lock object
     */
    @Override
    public Lock get(int id) {
        return lockRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing Lock
     *
     * @param entity Lock to create or update
     * @return Created or updated Lock object
     */
    @Override
    public Lock save(Lock entity) {
        return lockRepository.save(entity);
    }

    /**
     * Deletes the Lock
     *
     * @param entity Lock to delete
     */
    @Override
    public void delete(Lock entity) {
        lockRepository.delete(entity);
    }

    /**
     * Deletes a Lock using an Id
     *
     * @param id Id of a Lock
     */
    @Override
    public void deleteById(int id) {
        lockRepository.deleteById(id);
    }

    /**
     * Retrieves all Locks
     *
     * @return List of Lock objects
     */
    @Override
    public List<Lock> getAll() {
        return lockRepository.findAll();
    }
    //</editor-fold>

    /**
     * Retrieves all Locks of a specific Station
     *
     * @param stationId Id of the Station
     * @return List of Locks
     * @throws ResourceNotFoundException Thrown exception if the station was not found
     */
    public List<Lock> getAllByStation(int stationId) throws ResourceNotFoundException {
        Station station = stationService.get(stationId);
        if (station == null) throw new ResourceNotFoundException("Station Not Found");
        else return lockRepository.findAllByStation(station);
    }

    /**
     * Retrieves all Locks of a specific Station and with a vehicle available
     *
     * @param stationId Id of the Station
     * @return List of Locks
     * @throws ResourceNotFoundException Thrown exception if the station was not found
     */
    public List<Lock> getAllByStationWithVehicle(int stationId) throws ResourceNotFoundException {
        Station station = stationService.get(stationId);
        if (station == null) throw new ResourceNotFoundException("Station Not Found");
        else return lockRepository.findAllByStationAndVehicleIsNotNull(station);
    }

    /**
     * Retrieves all Locks of a specific Station and without a vehicle available
     *
     * @param stationId Id of the Station
     * @return List of Locks
     * @throws ResourceNotFoundException Thrown exception if the station was not found
     */
    public List<Lock> getAllByStationWithoutVehicle(int stationId) throws ResourceNotFoundException {
        Station station = stationService.get(stationId);
        if (station == null) throw new ResourceNotFoundException("Station Not Found");
        else return lockRepository.findAllByStationAndVehicleIsNull(station);
    }
}

