package be.kdg.sa.rideservice.domain;

import javax.persistence.*;
import java.util.List;

@Entity(name = "SubscriptionTypes")
public class SubscriptionType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SubscriptionTypeId", columnDefinition = "TINYINT")
    private int id;

    @Column(name = "Description", columnDefinition = "NVARCHAR", length = 50)
    private String description;

    @OneToMany(mappedBy = "subscriptionType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Subscription> subscriptions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }
}
