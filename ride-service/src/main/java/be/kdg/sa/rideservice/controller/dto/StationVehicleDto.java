package be.kdg.sa.rideservice.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class StationVehicleDto implements Serializable {
    private int userId;
    private int stationId;
    private int stationLockNr;
    private int lockId;
    private int vehicleId;

    public StationVehicleDto(int stationLockNr) {
        this.stationLockNr = stationLockNr;
    }

    public StationVehicleDto() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public int getStationLockNr() {
        return stationLockNr;
    }

    public void setStationLockNr(int stationLockNr) {
        this.stationLockNr = stationLockNr;
    }

    public int getLockId() {
        return lockId;
    }

    public void setLockId(int lockId) {
        this.lockId = lockId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }
}

