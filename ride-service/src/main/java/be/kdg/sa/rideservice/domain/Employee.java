package be.kdg.sa.rideservice.domain;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Employees")
public class Employee {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name = "EmployeeId", columnDefinition = "SMALLINT")
    private int id;

    @Column(name = "Name", columnDefinition = "NVARCHAR", length = 100, nullable = false)
    private String name;

    @Column(name = "ExperienceLevel", columnDefinition = "TINYINT", nullable = false)
    private int experienceLevel;

    @Column(name = "HourlyRate", columnDefinition = "TINYINT", nullable = false)
    private int hourlyRate;

    @Column(name = "Email", columnDefinition = "NVARCHAR", length = 100)
    private String email;

    @Column(name = "Street", columnDefinition = "NVARCHAR", length = 100)
    private String street;

    @Column(name = "Number", columnDefinition = "NVARCHAR", length = 10)
    private String number;

    @Column(name = "Zipcode", columnDefinition = "NVARCHAR", length = 10)
    private String zipcode;

    @Column(name = "City", columnDefinition = "NVARCHAR", length = 100)
    private String city;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Ride> rides;

    public Employee() {
        name = "unavailable";
        experienceLevel = 0;
        hourlyRate = 0;
    }

    public Employee(String name, int experienceLevel, int hourlyRate) {
        this.name = name;
        this.experienceLevel = experienceLevel;
        this.hourlyRate = hourlyRate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExperienceLevel() {
        return experienceLevel;
    }

    public void setExperienceLevel(int experienceLevel) {
        this.experienceLevel = experienceLevel;
    }

    public int getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(int hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Ride> getRides() {
        return rides;
    }

    public void setRides(List<Ride> rides) {
        this.rides = rides;
    }
}
