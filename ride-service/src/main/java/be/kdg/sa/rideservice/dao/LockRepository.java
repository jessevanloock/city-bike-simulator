package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.Lock;
import be.kdg.sa.rideservice.domain.Station;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LockRepository extends JpaRepository<Lock, Integer> {

    List<Lock> findAllByStation(Station station);
    List<Lock> findAllByStationAndVehicleIsNull(Station station);
    List<Lock> findAllByStationAndVehicleIsNotNull(Station station);

}
