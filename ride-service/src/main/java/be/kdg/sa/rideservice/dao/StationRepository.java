package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.Station;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StationRepository extends JpaRepository<Station, Integer> {
}
