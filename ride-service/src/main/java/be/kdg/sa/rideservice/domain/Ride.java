package be.kdg.sa.rideservice.domain;

<<<<<<< HEAD
import com.vividsolutions.jts.geom.Point;

=======
>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af
import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity(name = "Rides")
public class Ride {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RideId", columnDefinition = "BIGINT")
    private int id;

<<<<<<< HEAD
    @Column(name = "StartPoint")
    private Point startPoint;

    @Column(name = "EndPoint")
    private Point endPoint;
=======
    //@Column(name = "StartPoint")
    //private Point startPoint;

    //@Column(name = "EndPoint")
    //private Point endPoint;
>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af

    @Column(name = "StartTime")
    private Timestamp startTime;

    @Column(name = "EndTime")
    private Timestamp endTime;

    @ManyToOne
    @JoinColumn(name = "VehicleId", nullable = false)
    private Vehicle vehicle;

    @ManyToOne
    @JoinColumn(name = "SubscriptionId")
    private Subscription subscription;

    @ManyToOne
    @JoinColumn(name = "EmployeeId")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "StartLockid")
    private Lock startLock;

    @ManyToOne
    @JoinColumn(name = "EndLockId")
    private Lock endLock;

    public Ride() {
    }

    public Ride(Vehicle vehicle, Subscription subscription) {
        this.vehicle = vehicle;
        this.subscription = subscription;
        startTime = Timestamp.valueOf(LocalDateTime.now());
    }

    public Ride(Vehicle vehicle, Lock startLock, Subscription subscription) {
        this.vehicle = vehicle;
        this.startLock = startLock;
        this.subscription = subscription;
        startTime = Timestamp.valueOf(LocalDateTime.now());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

<<<<<<< HEAD
    public Point getStartPoint() {
=======
   /* public Point getStartPoint() {
>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af
        return startPoint;
    }

    public void setStartPoint(Point startPoint) {
        this.startPoint = startPoint;
    }

    public Point getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Point endPoint) {
        this.endPoint = endPoint;
<<<<<<< HEAD
    }
=======
    }*/
>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Lock getStartLock() {
        return startLock;
    }

    public void setStartLock(Lock startLock) {
        this.startLock = startLock;
    }

    public Lock getEndLock() {
        return endLock;
    }

    public void setEndLock(Lock endLock) {
        this.endLock = endLock;
    }
}
