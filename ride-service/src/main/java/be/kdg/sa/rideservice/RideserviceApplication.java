package be.kdg.sa.rideservice;

import be.kdg.sa.rideservice.domain.Vehicle;
import be.kdg.sa.rideservice.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
<<<<<<< HEAD
public class RideserviceApplication implements CommandLineRunner {

	// TODO: Clear this application again, only used for testing at this point

	private final UserService userService;
	private final VehicleService vehicleService;
	private final EmployeeService employeeService;
	private final LockService lockService;
	private final RideService rideService;
	private final StationService stationService;
	private final SubscriptionService subscriptionService;

	@Autowired
	public RideserviceApplication(UserService userService, VehicleService vehicleService, EmployeeService employeeService, LockService lockService, RideService rideService, StationService stationService, SubscriptionService subscriptionService) {
		this.userService = userService;
		this.vehicleService = vehicleService;
		this.employeeService = employeeService;
		this.lockService = lockService;
		this.rideService = rideService;
		this.stationService = stationService;
		this.subscriptionService = subscriptionService;
	}

=======
public class RideserviceApplication implements CommandLineRunner{
	private final VehicleService vehicleService;
	private final UserService	userService;
	private final RideService	rideService;
	private final SubscriptionService	subscriptionService;

	private final StationService stationService;

	@Autowired
	public RideserviceApplication(VehicleService vehicleService, UserService userService, RideService rideService, SubscriptionService subscriptionService, StationService stationService) {
		this.vehicleService = vehicleService;
		this.userService = userService;
		this.subscriptionService = subscriptionService;
		this.stationService = stationService;
		this.rideService = rideService;
	}


>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af
	public static void main(String[] args) {
		SpringApplication.run(RideserviceApplication.class, args);

	}


	@Override
	public void run(String... args) throws Exception {
		// This might take a few seconds as we're basically reading the entire database
		Vehicle vehicle = vehicleService.get(1);
		vehicleService.getAll().forEach(v -> System.out.println(v.getPoint()));
		System.out.println(
				"DATABASE TESTING ==================" +
						"\n* Users found: " + userService.getAll().size() +
						"\n* Vehicles found: " + vehicleService.getAll().size() +
						"\n* Bikelots found: " + vehicleService.getAllBikelots().size() +
						"\n* BikeMaintenanceTypes found: " + vehicleService.getAllMaintenanceTypes().size() +
						"\n* BikeTypes found: " + vehicleService.getAllBikeTypes().size() +
						"\n* Employees found: " + employeeService.getAll().size() +
						"\n* Locks found: " + lockService.getAll().size() +
						//"\n* Rides found: " + rideService.getAllRides().size() +
						"\n* Stations found: " + stationService.getAll().size() +
						"\n* Subscriptions found: " + subscriptionService.getAll().size() +
						"\n* SubscriptionTypes found: " + subscriptionService.getAllTypes().size() +
						"\n===================================");
		/*Subscription s = rideService.findValidSubscriptionOfUser(6);
		if (s != null) System.out.println("Valid subscription found (id = "+ s.getId() +")");
		else System.out.println("No valid subscription found!");*/


		/*System.out.println("Starting a new ride at station 1 with user 6...");
		System.out.println("Available vehicles found at station: "+ rideService.getAllLocksByStationWithVehicle(1).size());
		System.out.println("Available vehicle at stationLockNr: "+ rideService.getAllLocksByStationWithVehicle(1).get(0).getStationLockNr());
		System.out.println("Valid subscription with id: "+ rideService.findValidSubscriptionOfUser(6).getId());
		Ride ride = rideService.startNewRide(6, 1);
		System.out.println("New ride started, validating now...");
		System.out.println("Available vehicles found at station: "+ rideService.getAllLocksByStationWithVehicle(1));
		if (ride != null) {
			System.out.println("Ride started successfully");
			System.out.println("id: "+ ride.getId());
			System.out.println("startTime: "+ ride.getStartTime());
			System.out.println("startLock: "+ ride.getStartLock().getStationLockNr());
			System.out.println("vehicle: "+ ride.getVehicle().getId());
		} else {
			System.out.println("Something went wrong!");
		}*/

	}

	@Override
	public void run(String... args) throws Exception {
		Bikelot bikelot = vehicleService.getBikelot(1);
		Vehicle vehicle = vehicleService.get(1);
		Subscription subscription = subscriptionService.get(1);
		Station station = stationService.get(1);
		Ride ride = rideService.get(1663540);
		System.out.println("hey");
	}
}