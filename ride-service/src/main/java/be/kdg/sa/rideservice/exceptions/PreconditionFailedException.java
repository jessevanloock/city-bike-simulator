package be.kdg.sa.rideservice.exceptions;

public class PreconditionFailedException extends Exception {
    public PreconditionFailedException(String message) {
        super(message);
    }
}
