package be.kdg.sa.rideservice.dao;

import be.kdg.sa.rideservice.domain.Bikelot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BikelotRepository extends JpaRepository<Bikelot, Integer> {
}
