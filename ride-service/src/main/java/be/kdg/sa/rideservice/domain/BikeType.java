package be.kdg.sa.rideservice.domain;

import javax.persistence.*;
import java.util.List;

@Entity(name = "BikeTypes")
public class BikeType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BikeTypeId", columnDefinition = "TINYINT")
    private int id;

    @Column(name = "BikeTypeDescription", columnDefinition = "NVARCHAR", length = 200)
    private String description;

    @OneToMany(mappedBy = "bikeType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Bikelot> bikelots;

    public BikeType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Bikelot> getBikelots() {
        return bikelots;
    }

    public void setBikelots(List<Bikelot> bikelots) {
        this.bikelots = bikelots;
    }
}
