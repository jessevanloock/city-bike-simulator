package be.kdg.sa.rideservice.service;

import be.kdg.sa.rideservice.dao.RideRepository;
import be.kdg.sa.rideservice.domain.Lock;
import be.kdg.sa.rideservice.domain.Ride;
import be.kdg.sa.rideservice.domain.Subscription;
import be.kdg.sa.rideservice.domain.Vehicle;
import be.kdg.sa.rideservice.exceptions.PreconditionFailedException;
import be.kdg.sa.rideservice.exceptions.ResourceNotFoundException;
import be.kdg.sa.rideservice.service.generic.IService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * RideService is a class used for all logic
 * required to use the class RideRepository directly
 * and concerning Rides
 *
 * @author Jonas Marien
 * @version 1.0
 * @since 01-10-2019
 */
@Service
public class RideService implements IService<Ride> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleService.class);

    private final RideRepository rideRepository;

    private final UserService userService;
    private final LockService lockService;
    private final VehicleService vehicleService;


    /**
     * Constructor for class RideService
     *  @param rideRepository RideRepository class used to access the dao layer of this application
     * @param userService UserService class used to access User logic
     * @param lockService LockService class used to access Lock logic
     * @param vehicleService VehicleService class used to access Vehicle logic
     */
    @Autowired
    public RideService(RideRepository rideRepository,
                       UserService userService,
                       LockService lockService,
                       VehicleService vehicleService) {
        this.rideRepository = rideRepository;
        this.userService = userService;
        this.lockService = lockService;
        this.vehicleService = vehicleService;
    }

    //<editor-fold desc="CRUD">

    /**
     * Retrieves a Ride using an id
     *
     * @param id Id of the Ride
     * @return Ride object
     */
    @Override
    public Ride get(int id) {
        return rideRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing Ride
     *
     * @param entity Ride to create or update
     * @return Created or updated Ride object
     */
    @Override
    public Ride save(Ride entity) {
        return rideRepository.save(entity);
    }

    /**
     * Deletes the Ride
     *
     * @param entity Ride to delete
     */
    @Override
    public void delete(Ride entity) {
        rideRepository.delete(entity);
    }

    /**
     * Deletes a Ride using an Id
     *
     * @param id Id of a Ride
     */
    @Override
    public void deleteById(int id) {
        rideRepository.deleteById(id);
    }

    /**
     * Retrieves all Rides
     *
     * @return List of Ride objects
     */
    @Override
    public List<Ride> getAll() {
        return rideRepository.findAll();
    }
    //</editor-fold>

    /**
     * Unlocks a Vehicle at a Station and starts a Ride for a given User
     *
     * @param userId Id of the User
     * @param stationId Id of the Station
     * @return Ride object
     * @throws ResourceNotFoundException Thrown if Station or User were not found
     * @throws PreconditionFailedException Thrown if User already has a Ride active
     */
    public Ride startRideWithStationVehicle(int userId, int stationId)
            throws ResourceNotFoundException, PreconditionFailedException {

        Subscription subscription = userService.findValidSubscriptionOfUser(userId);

        boolean hasActiveRide = hasActiveRide(subscription);
        if (hasActiveRide)
            throw new PreconditionFailedException("User already has a ride active");

        List<Lock> locksWithVehicle = lockService.getAllByStationWithVehicle(stationId);
        if (!locksWithVehicle.isEmpty()) {
            Lock lock = locksWithVehicle.get(0);
            Vehicle vehicle = lock.getVehicle();
            lock.setVehicle(null);

            // TODO: these should be as one transaction
            lock = lockService.save(lock);
            return rideRepository.saveAndFlush(new Ride(vehicle, lock, subscription)); // TODO: Exception handling
        } else return null;
    }

    /**
     * Locks a Vehicle at a Lock and ends the linked Ride for a given User
     *
     * @param userId Id of the User
     * @param lockId Id of the Lock
     * @throws ResourceNotFoundException Thrown if Lock or User were not found
     * @throws PreconditionFailedException Thrown if User has no Ride active
     */
    public void endRideWithStationVehicle(int userId, int lockId)
            throws ResourceNotFoundException, PreconditionFailedException {

        Subscription subscription = userService.findValidSubscriptionOfUser(userId);
        boolean hasActiveRide = hasActiveRide(subscription);
        if (!hasActiveRide)
            throw new PreconditionFailedException("User already has no ride active");

        Ride ride = getLastRideBySubscription(subscription);
        if (ride != null) {
            Lock lock = lockService.get(lockId);

            Vehicle vehicle = ride.getVehicle();
            vehicle.setLockId(lock.getId());
            vehicleService.save(vehicle);

            lock.setVehicle(null);
            lockService.save(lock);

            ride.setEndTime(Timestamp.valueOf(LocalDateTime.now()));
            ride.setEndLock(lock);

            rideRepository.save(ride); // TODO: Exception handling


        } else {
            // TODO: Exception handling
        }
    }

    /**
     * Starts a new Ride with a Vehicle for a given User
     *
     * @param userId Id of the User
     * @param vehicleId Id of the Vehicle
     * @return Ride object
     * @throws ResourceNotFoundException Thrown if Vehicle or User were not found
     * @throws PreconditionFailedException Thrown if User already has a Ride active
     */
    public Ride startRideWithFreeVehicle(int userId, int vehicleId)
            throws ResourceNotFoundException, PreconditionFailedException {

        Subscription subscription = userService.findValidSubscriptionOfUser(userId);
        boolean hasActiveRide = hasActiveRide(subscription);
        if (hasActiveRide)
            throw new PreconditionFailedException("User already has a ride active");

        Vehicle vehicle = vehicleService.get(vehicleId);
        if (vehicle != null) return rideRepository.save(new Ride(vehicle, subscription));
        else throw new ResourceNotFoundException("Vehicle Not Found");
    }

    /**
     * Ends an existing Ride with a Vehicle for a given User
     *
     * @param userId Id of the User
     * @param vehicleId Id of the Vehicle
     * @return Ride object
     * @throws ResourceNotFoundException Thrown if Vehicle or User were not found
     * @throws PreconditionFailedException Thrown if User has no Ride active
     */
    public Ride endRideWithFreeVehicle(int userId, int vehicleId)
            throws ResourceNotFoundException, PreconditionFailedException {

        Subscription subscription = userService.findValidSubscriptionOfUser(userId);
        boolean hasActiveRide = hasActiveRide(subscription);
        if (!hasActiveRide)
            throw new PreconditionFailedException("User already has no ride active");

        Ride ride = getLastRideBySubscription(subscription);
        if (ride != null) {
            if (ride.getVehicle().getId() != vehicleId) return null;

            ride.setEndTime(Timestamp.valueOf(LocalDateTime.now()));
            // TODO: setLocation and calculate prices
            return rideRepository.saveAndFlush(ride);

        } else {
            return null; // TODO: Exception handling
        }
    }

    /**
     * Checks if a User has a Ride active using a Subscription
     *
     * @param subscription Subscription object of the User
     * @return boolean
     */
    public boolean hasActiveRide(Subscription subscription) {
        List<Ride> activeRides = rideRepository.findAllBySubscriptionAndEndTimeIsNull(subscription);
        return !activeRides.isEmpty();
    }

    /**
     * Retrieves the active Ride of a User using a Subscription
     *
     * @param subscription Subscription object of te User
     * @return Ride object
     */
    public Ride getLastRideBySubscription(Subscription subscription) {
        List<Ride> activeRides = rideRepository.findAllBySubscriptionAndEndTimeIsNull(subscription);

        if (activeRides.isEmpty()) return null;
        else return activeRides.get(activeRides.size() - 1);
    }

    /**
     * Detects an open Ride based on time
     */
    public void detectOpenRideTimeBased(){
        // TODO: # Minutes should be read from a config file
        rideRepository.findAllByEndTimeIsNullAndStartTimeIsBefore(
                Timestamp.valueOf(LocalDateTime.now().minusMinutes(30)))
                .forEach(r -> System.out.printf("[%s] [Time based] Open ride detected {id=%d}%n",
                        Timestamp.valueOf(LocalDateTime.now()),
                        r.getId()));
    }
}

