package be.kdg.sa.rideservice.domain;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Locks")
public class Lock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "LockId", columnDefinition = "SMALLINT")
    private int id;

    @Column(name = "StationLockNr", columnDefinition = "TINYINT", nullable = false)
    private int stationLockNr;

    @ManyToOne
    @JoinColumn(name = "StationId", nullable = false)
    private Station station;

    @ManyToOne
    @JoinColumn(name = "VehicleId")
    private Vehicle vehicle;

<<<<<<< HEAD
    @OneToMany(mappedBy = "startLock", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Ride> startRides;
=======
    @OneToMany(mappedBy = "startLock", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Ride> startRides;

    @OneToMany(mappedBy = "endLock", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Ride> endRides;
>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af

    @OneToMany(mappedBy = "endLock", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Ride> endRides;

    public Lock() {
        stationLockNr = 0;
    }

    public Lock(int stationLockNr) {
        this.stationLockNr = stationLockNr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStationLockNr() {
        return stationLockNr;
    }

    public void setStationLockNr(int stationLockNr) {
        this.stationLockNr = stationLockNr;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

<<<<<<< HEAD
=======
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStationLockNr() {
        return stationLockNr;
    }

    public void setStationLockNr(int stationLockNr) {
        this.stationLockNr = stationLockNr;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

>>>>>>> f79b6991994a222f78e66d35936e1116a3bd64af
    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public List<Ride> getStartRides() {
        return startRides;
    }

    public void setStartRides(List<Ride> startRides) {
        this.startRides = startRides;
    }

    public List<Ride> getEndRides() {
        return endRides;
    }

    public void setEndRides(List<Ride> endRides) {
        this.endRides = endRides;
    }
}
