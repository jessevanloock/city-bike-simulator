package be.kdg.sa.rideservice.service;

import be.kdg.sa.rideservice.dao.UserRepository;
import be.kdg.sa.rideservice.domain.Subscription;
import be.kdg.sa.rideservice.domain.User;
import be.kdg.sa.rideservice.exceptions.ResourceNotFoundException;
import be.kdg.sa.rideservice.service.generic.IService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

/**
 * UserService is a class used for all logic
 * required to use the class UserRepository directly
 * and concerning Users
 *
 * @author Jonas Marien
 * @version 1.0
 * @since 01-10-2019
 */
@Service
@Transactional
public class UserService implements IService<User> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleService.class);

    private final UserRepository userRepository;

    /**
     * Constructor for class UserService
     *
     * @param userRepository UserRepository class used to access the dao layer of this application
     */
    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    //<editor-fold desc="CRUD">

    /**
     * Retrieves a User using an id
     *
     * @param id Id of the User
     * @return User object
     */
    @Override
    public User get(int id) {
        return userRepository.findById(id).orElse(null);
    }

    /**
     * Creates a new or updates an existing User
     *
     * @param entity User to create or update
     * @return Created or updated User object
     */
    @Override
    public User save(User entity) {
        return userRepository.save(entity);
    }

    /**
     * Deletes the User
     *
     * @param entity User to delete
     */
    @Override
    public void delete(User entity) {
        userRepository.delete(entity);
    }

    /**
     * Deletes a User using an Id
     *
     * @param id Id of a User
     */
    @Override
    public void deleteById(int id) {
        userRepository.deleteById(id);
    }

    /**
     * Retrieves all Users
     *
     * @return List of User objects
     */
    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
    //</editor-fold>

    /**
     * Retrieves and checks if a User has a valid Subscription
     *
     * @param userId Id of the User
     * @return Subscription object
     * @throws ResourceNotFoundException Thrown if User was not found
     */
    public Subscription findValidSubscriptionOfUser(int userId) throws ResourceNotFoundException {
        User userToValidate = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User Not Found"));

        for (Subscription subscription: userToValidate.getSubscriptions()) {
            switch (subscription.getSubscriptionType().getId()) {
                case 1: // 1 day subscription
                    if (LocalDate.now()
                            .minusDays(1)
                            .isBefore(subscription.getValidFrom().toLocalDate())) return subscription;
                    break;
                case 2: // 1 month subscription
                    if (LocalDate.now()
                            .minusMonths(1)
                            .isBefore(subscription.getValidFrom().toLocalDate())) return subscription;
                    break;
                case 3: // 1 year subscription
                    if (LocalDate.now()
                            .minusYears(1)
                            .isBefore(subscription.getValidFrom().toLocalDate())) return subscription;
                    break;
            }
        }
        return null;
    }
}

