package be.kdg.sa.rideservice.controller;

import be.kdg.sa.rideservice.controller.dto.StationVehicleDto;
import be.kdg.sa.rideservice.domain.Vehicle;
import be.kdg.sa.rideservice.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * VehicleRestController class used for receiving and handling API-calls
 * only CRUD on Vehicle objects
 *
 * @author Jonas Marien
 * @version 1.0
 * @since 04-10-2019
 */
@RestController
public class VehicleRestController {
    private final VehicleService vehicleService;

    /**
     * Constructor for VehicleRestController class
     *
     * @param vehicleService VehicleService used to handle CRUD actions on Vehicle objects
     */
    @Autowired
    public VehicleRestController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    /**
     * GET method on all Vehicles
     *
     * @return List of Vehicle objects
     */
    @GetMapping("/vehicles")
    public List<Vehicle> getAllVehicles() {
        return vehicleService.getAll();
    }

    /**
     * GET method on all Vehicles with a Station
     *
     * @return List of Vehicle objects
     */
    @GetMapping("/stationvehicles")
    public List<Vehicle> getAllStationVehicles() {
        return vehicleService.getAllStationVehicles();
    }

    /**
     * GET method on all Vehicles without a Station
     *
     * @return List of Vehicle objetc
     */
    @GetMapping("/freevehicles")
    public List<Vehicle> getAllFreeVehicles() {
        return vehicleService.getAllFreeVehicles();
    }

    /**
     * GET method on a single Vehicle using an Id
     *
     * @param id Id of a Vehicle
     * @return Vehicle object
     */
    @GetMapping("/vehicles/{id}")
    public StationVehicleDto getVehicle(@PathVariable int id) {
        Vehicle vehicle = vehicleService.get(id);
        StationVehicleDto dto = new StationVehicleDto();
        dto.setLockId(vehicle.getLockId());
        dto.setVehicleId(vehicle.getId());
        return dto;
    }

    /**
     * DELETE method on a single Vehicle using an Id
     *
     * @param id Id of a Vehicle
     */
    @DeleteMapping("/vehicles/{id}")
    public void deleteVehicle(@PathVariable int id) {
        vehicleService.deleteById(id);
    }

    /**
     * POST method on a single Vehicle to create a new Vehicle
     *
     * @param vehicle Vehicle object to create
     * @return Created Vehicle object
     */
    @PostMapping("/vehicles")
    public ResponseEntity<Vehicle> createVehicle(@RequestBody Vehicle vehicle){
        try {
            Vehicle createdVehicle = vehicleService.save(vehicle);
            return new ResponseEntity<>(createdVehicle, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    /**
     * PUT method on a single Vehicle to update an existing Vehicle
     *
     * @param vehicle Vehicle object to update
     * @param id Id of the Vehicle to update
     * @return Updated Vehicle object
     */
    @PutMapping("/vehicles/{id}")
    public ResponseEntity<Vehicle> updateVehicle(@RequestBody Vehicle vehicle, @PathVariable int id){
        Vehicle vehicleToUpdate = vehicleService.get(id);
        if (vehicleToUpdate != null){
            try {
                vehicle.setId(id);
                vehicleToUpdate = vehicleService.save(vehicle);
                return new ResponseEntity<>(vehicleToUpdate, HttpStatus.ACCEPTED);
            } catch (Exception e) {
                return new ResponseEntity<>(vehicle, HttpStatus.NOT_MODIFIED);
            }
        } else {
            return new ResponseEntity<>(vehicle, HttpStatus.NOT_FOUND);
        }
    }
}

