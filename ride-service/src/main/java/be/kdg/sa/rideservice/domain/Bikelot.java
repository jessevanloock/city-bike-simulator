package be.kdg.sa.rideservice.domain;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Entity(name = "Bikelots")
public class Bikelot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BikeLotId", columnDefinition="SMALLINT")
    private int id;

    @Column(name = "DeliveryDate")
    private Date deliveryDate;

    @OneToMany(mappedBy = "bikelot", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Vehicle> vehicles;

    @ManyToOne
    @JoinColumn(name = "BikeTypeId", nullable = false)
    private BikeType bikeType;

    public Bikelot() {
        deliveryDate = Date.valueOf(LocalDate.now());
    }

    public Bikelot(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public BikeType getBikeType() {
        return bikeType;
    }

    public void setBikeType(BikeType bikeType) {
        this.bikeType = bikeType;
    }
}
