### Zelfreflectie 30/09/2019 - Pre
------------------------------
Jesse Van Loock:  
Wat ging goed:  
	- Goed begrip van de opdracht  
	- Groot deel van sensor simulatie afgewerkt  
	- H2 Databank toegevoegd aan SensorService  
Wat ging minder goed:  
	- Niet goed weten wat Spring allemaal kan doen  
	- Eerste 2 weken veel opzoekwerk en tutorials gedaan  
Vragen:  
	- Package-structuur? 
	
Jonas Marien:  
Wat ging goed:  
	- Verdeling van het werk   
	- Functioneel gebaseerd programmeren (focus op Logic First, nog geen exception handling of dergelijke)    
	- Opbouw basis-structuur (Dao, Service, Controller)  
Wat ging minder goed:  
	- Persistentie probleem bij Lock en Vehicle entiteiten (OneToOne mapping, opgelost)  
Vragen:  
	- RideService en RideServiceController verder opsplitsen gebaseerd op functionaliteit?   
	
### Zelfreflectie 30/09/2019 - Post
------------------------------
Al gedaan:  
	- simulator genereert random sensor messages  
	- message queues voor sensor service  
	- H2 Databank toegevoegd  
	- RideService databank persistentie  
Nog te doen:  
	- thymeleaf van simulator uitwerken  
	- csv-simulatie  
	- Date naar DateTime in simulator  
	- objectmapper bijsturen in simulator  
	- queue, maar in property file (is nog hardcoded)  
	- properties uit generator halen (aparte klasse)  
	- lezende kant van de sensor service  
	- thymeleaf van sensor service  
	- controllers uitwerken in de ride service  
	- api calls in ride service  
	
### Zelfreflectie 20/10/2019 - Pre
------------------------------
Status:  
	- Functionaliteit zo goed als klaar  
	- Vooral nog wat werk aan kleine fouten, details en refactoring  
	- Jonas heeft RideService uitgewerkt & Jesse heeft Simulator en SensorService uitgewerkt  
	- Ongeveer 80% van het project is klaar  
	- Samenwerking is vlot verlopen -> We weten wat er van elkaar verwacht wordt  
	
Simulator - Afgewerkte Issues:  
	- 52: Spring boot project opzetten  
	- 33: Project versiebeheer in GitLab opzetten  
	- 18: Random sensor berichten genereren  
	- 31: Queue en andere settings kunnen beheren in configuratiebestand  
	- 19: Een sensor simulator starten via de webinterface  
	- 48: Een sensor simulatie configureren via web interface  
	- 47: Een Ride CSV opladen via web interface  
	- 17: Een ride simulatie starten via de webinterface  
	- 15: Simuleren van rit op basis van een csv-bestand  
	- 16: In een ride simulatie delays gebruiken die in het csv-bestand worden meegegeven  
	
Simulator - Nog te doen:
	- 32: Logging en foutafhandeling correct toepassen  
	- Returnwaarden van API calls gebruiken  
	- UI  
	- Aparte Threads  
	- Refactoring  
	
SensorService - Afgewerkte Issues:  
	- 54: Spring boot project opzetten  
	- 44: Project versiebeheer in GitLab opzetten  
	- 6 : De luchtkwaliteit opvolgen op basis van een lijst  
	- 8 : De luchtkwaliteit opvolgen op basis van een lijn-grafiek  
	- 9 : De luchtkwaliteit opvolgen op basis van een heatmap  
	- 42: Queue, databank en andere settings kunnen beheren in configuratiebestand  
	
SensorService - Nog te doen:  
	- 7 : Luchtkwaliteit bericht registreren in H2 database
	- 22: Bij opvolging luchtkwaliteit filteren op type meting, tijd en locatie  
	- 46: Logging en foutafhandeling correct toepassen  
	- Documentation  
	- UI  
	- Refactoring  
	
RideService - Afgewerkte Issues:  
	- 52: Spring boot project opzetten  
	- 43: Project versiebeheer in GitLab opzetten  
	- 55: SQL Server 2017 database opzetten en script inladen  
	- 23: Bikes beheren (CRUD) via een rest API  
	- 51: API testen  
	- 50: Unit test van open ride detectie  
	- 28: Open ride detecteren als een rit ongewoon lang duurt  
	- 27: Opgehaalde pricing details cachen  
	- 38: CloudAMQP als message queue client opzetten  
	- 26: Pricing details ophalen bij pricing service  
	- 12: Ritprijs berekenen  
	- 5 : De ontvangen locatie van een "free vehicle" verwerken  
	- 10: De dichtst bijzijnde "free vehicle" opvragen  
	- 1 : Ride met een "station vehicle" starten  
	- 56: Free locks voor een station ophalen  
	- 2 : Ride met een "free vehicle" starten  

RideService - Nog te doen:  
	- 4 : Ride met een "free vehicle" beëindigen  
	- 45: Logging en foutafhandeling correct toepassen  
	- 52: Code-impact van ander prijs berekening mechanisme beperken  
	- 49: Unit test van de prijsberekening met mocking van Pricing Service  
	- 3 : Ride met een "station vehicle" beëindigen  
	- 41: Queue, databank en andere settings kunnen beheren in configuratiebestand  
	- 11: Ritprijs (in XML formaat) doorsturen naar Invoice systeem  
	- 20: Code-impact van ander dan json formaat voor ride events beperken  
	- 29: Open ride detecteren als een free vehicle ongewoon lang opdelfde locatie blijft  
	- 30: Code-impact van ander open ride detectiemechanisme beperken  
	
Quality:  
	- Refactoring  
	- Documentation  
	- Propere UI  
	- Foutafhandeling  
	- Logging  
	- Eventuele uitbreidingen  

Vragen:  
	- SensorService -> Wat wordt verwacht van filteren op plaats?  

### Zelfreflectie 21/10/2019 - Post
------------------------------

Nog te doen:
- Thymeleaf refactoring van JS (JS in aparte file)
- Data meegeven via RestController ipv Model
- POST vervangen door een GET ('/sensormessages')
- refactoring van de meeste code
- @async gebruiken bij methodes voor threading
- hardcoded strings, zoals queuenaam en urls, in properties zetten
- JsonBuilder als @Bean noteren in een @Configuration klasse voor DI
- 'processCSVline' ipv 'chooseMethod' -> datatypes al casten in deze methode
- @ControllerAdvice gebruiken voor Exception handling op Controller-niveau
- Bij Create methode de DTO ombouw vervangen door een save methode met meerdere parameters
- @Cacheable gebruiken in de PriceService
- Bij openRide detectie (locatie) een map gebruiken op UserId en Points voor historiek bij te houden

### Zelfreflectie 04/11/2019 - Pre
------------------------------
Status:  
	- Alle Functionalisteit en enkele uitbreidingen uitgewerkt    
	- Feedback van vorige statusmeetingen toegepast  
	- Jonas heeft RideService uitgewerkt & Jesse heeft Simulator en SensorService uitgewerkt    
	- Samenwerking is vlot verlopen  
	
Wat ging goed:  
	- Single Responsibility Principle zo vaak mogelijk gebruiken  
	- Messages op queue zetten en uit queue lezen  
	- REST 
	- Gebruik van thymeleaf  
	- Gebruik maken van Beans en Dependency Injection  
	- Instellingen beheren in application.properties  
	
Wat ging minder goed:  
	- /

### Zelfreflectie 21/10/2019 - Post
------------------------------

Nog te doen:
- Genieten van de vakantie