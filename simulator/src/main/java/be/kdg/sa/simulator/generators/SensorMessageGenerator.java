package be.kdg.sa.simulator.generators;

import be.kdg.sa.simulator.handlers.SensorMessageHandler;
import be.kdg.sa.simulator.model.SensorMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * SensorMessageGenerator class used for creating random SensorMessages based
 * on the settings in the application.properties file
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 28-09-2019
 */

@Component
public class SensorMessageGenerator {

    private final SensorMessageHandler handler;

    /**
     * Constructor for class SensorMessageGenerator
     *
     * @param handler SensorMessageHandler class used to call handleMessage method on the SensorMessage
     */
    public SensorMessageGenerator(SensorMessageHandler handler) {
        this.handler = handler;
    }

    /**
     * Uses the properties of GeneratorSettings to create random SensorMessages and handles the messages with the
     * help of the SensorMessageHandler
     *
     * @param settings GeneratorSettings used for the generation of SensorMessages and is based on the
     * configuration file application.properties
     */
    @Async
    public void generateSensorMessages(GeneratorSettings settings) throws InterruptedException {
        long currentTime = System.currentTimeMillis();
        long end = currentTime + settings.getTimeperiod();
        while(System.currentTimeMillis() < end) {
            int randomVariance = ThreadLocalRandom.current().nextInt(settings.getMean_delay()-settings.getDelay_variance(), settings.getMean_delay() + settings.getDelay_variance() + 1);
            double randomX = ThreadLocalRandom.current().nextDouble(settings.getX_min(), settings.getX_max() + 1);
            double randomY = ThreadLocalRandom.current().nextDouble(settings.getY_min(), settings.getY_max() + 1);
            double randomValue = ThreadLocalRandom.current().nextDouble(settings.getValue_min(), settings.getValue_max() + 1);
            Random rand = new Random();
            String randomType = settings.getTypes().get(rand.nextInt(settings.getTypes().size()));
            SensorMessage msg = new SensorMessage(
                    LocalDateTime.now(),
                    randomX,
                    randomY,
                    randomType,
                    randomValue
            );
            handler.handleMessage(msg);
            TimeUnit.MILLISECONDS.sleep(randomVariance);
        }
    }
}
