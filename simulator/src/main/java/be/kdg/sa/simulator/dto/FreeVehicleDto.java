package be.kdg.sa.simulator.dto;


public class FreeVehicleDto {
    private int vehicleId;
    private int userId;
    private double xCoord;
    private double yCoord;
    private String vehicleType;

    public FreeVehicleDto() {
    }

    public FreeVehicleDto(int vehicleId, double xCoord, double yCoord) {
        this.vehicleId = vehicleId;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getxCoord() {
        return xCoord;
    }

    public void setxCoord(double xCoord) {
        this.xCoord = xCoord;
    }

    public double getyCoord() {
        return yCoord;
    }

    public void setyCoord(double yCoord) {
        this.yCoord = yCoord;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
}
