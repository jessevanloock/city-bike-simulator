package be.kdg.sa.simulator.model;

import java.time.LocalDateTime;

/**
 * SensorMessage class used for creating a message to send to SensorService via a Queue
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 25-09-2019
 */

public class SensorMessage {
    private final LocalDateTime timeStamp;
    private final double xCoord;
    private final double yCoord;
    private final String type;
    private final double value;

    public SensorMessage(LocalDateTime timeStamp, double xCoord, double yCoord, String type, double value) {
        this.timeStamp = timeStamp;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.type = type;
        this.value = value;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public double getxCoord() {
        return xCoord;
    }

    public double getyCoord() {
        return yCoord;
    }

    public String getType() {
        return type;
    }

    public double getValue() {
        return value;
    }
}
