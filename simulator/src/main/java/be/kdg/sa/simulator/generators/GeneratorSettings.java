package be.kdg.sa.simulator.generators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * GeneratorSettings class used in SensorMessageGenerator and values of properties are from application.properties
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 28-09-2019
 */

@Component
public class GeneratorSettings {
    @Value("${sensor.timeperiod}")
    private int timeperiod;

    @Value("${sensor.delay.mean}")
    private int mean_delay;

    @Value("${sensor.delay.variance}")
    private int delay_variance;

    @Value("${sensor.x.min-value}")
    private double x_min;

    @Value("${sensor.x.max-value}")
    private double x_max;

    @Value("${sensor.y.min-value}")
    private double y_min;

    @Value("${sensor.y.max-value}")
    private double y_max;

    @Value("${sensor.types}")
    private List<String> types;

    @Value("${sensor.types.min-value}")
    private double value_min;

    @Value("${sensor.types.max-value}")
    private double value_max;

    public int getTimeperiod() {
        return timeperiod;
    }

    public int getMean_delay() {
        return mean_delay;
    }

    public int getDelay_variance() {
        return delay_variance;
    }

    public double getX_min() {
        return x_min;
    }

    public double getX_max() {
        return x_max;
    }

    public double getY_min() {
        return y_min;
    }

    public double getY_max() {
        return y_max;
    }

    public List<String> getTypes() {
        return types;
    }

    public double getValue_min() {
        return value_min;
    }

    public double getValue_max() {
        return value_max;
    }

    public void setTimeperiod(int timeperiod) {
        this.timeperiod = timeperiod;
    }

    public void setMean_delay(int mean_delay) {
        this.mean_delay = mean_delay;
    }

    public void setDelay_variance(int delay_variance) {
        this.delay_variance = delay_variance;
    }

    public void setX_min(double x_min) {
        this.x_min = x_min;
    }

    public void setX_max(double x_max) {
        this.x_max = x_max;
    }

    public void setY_min(double y_min) {
        this.y_min = y_min;
    }

    public void setY_max(double y_max) {
        this.y_max = y_max;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public void setValue_min(double value_min) {
        this.value_min = value_min;
    }

    public void setValue_max(double value_max) {
        this.value_max = value_max;
    }
}
