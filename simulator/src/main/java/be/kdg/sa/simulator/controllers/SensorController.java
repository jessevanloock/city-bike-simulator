package be.kdg.sa.simulator.controllers;

import be.kdg.sa.simulator.generators.GeneratorSettings;
import be.kdg.sa.simulator.generators.SensorMessageGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * SensorController class used for displaying the views for configuring and starting the sensor simulation
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 28-09-2019
 */
@Controller
public class SensorController {

    private final SensorMessageGenerator generator;
    private final GeneratorSettings settings;

    /**
     * Constructor for SensorController
     *
     * @param generator SensorMessageGenerator used to generate random SensorMessages on simulation start
     * @param settings GeneratorSettings used to show settings of Generator on form and to create new settings
     */
    public SensorController(SensorMessageGenerator generator, GeneratorSettings settings) {
        this.generator = generator;
        this.settings = settings;
    }

    /**
     * Shows the form with the settings of the SensorMessageGenerator and returns the "sensor" view.
     *
     * @param model Model used to add the settings to the model and pass the model to the view
     */
    @GetMapping("/sensor")
    public String sensorForm(Model model) {
        model.addAttribute("settings", settings);
        return "sensor";
    }

    /**
     * Shows the "sensorstart" view when form is submitted and calls the generateSensorMessages
     * from SensorMessageGenerator with the values of the form
     *
     * @param settings GeneratorSettings used to save values of form
     */
    @PostMapping("/sensor")
    public String sensorSubmit(@ModelAttribute("settings") GeneratorSettings settings) throws InterruptedException {
        generator.generateSensorMessages(settings);
        return "sensorstart";
    }

}
