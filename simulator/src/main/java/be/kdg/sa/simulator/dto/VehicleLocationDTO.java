package be.kdg.sa.simulator.dto;



public class VehicleLocationDTO {
    private String timestamp;
    private int vehicleId;
    private double xCoord;
    private double yCoord;

    public VehicleLocationDTO(String timestamp, int vehicleId, double xCoord, double yCoord) {
        this.timestamp = timestamp;
        this.vehicleId = vehicleId;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public double getxCoord() {
        return xCoord;
    }

    public void setxCoord(double xCoord) {
        this.xCoord = xCoord;
    }

    public double getyCoord() {
        return yCoord;
    }

    public void setyCoord(double yCoord) {
        this.yCoord = yCoord;
    }
}
