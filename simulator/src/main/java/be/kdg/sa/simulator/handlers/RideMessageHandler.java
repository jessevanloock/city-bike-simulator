package be.kdg.sa.simulator.handlers;

import be.kdg.sa.simulator.dto.VehicleLocationDTO;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * RideMessageHandler class used for creating a queue and sending messages on this queue
 * to RideService
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 07-10-2019
 */

@Component
public class RideMessageHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RideMessageHandler.class);
    @Value("${ridequeue}")
    private String rideQueueName;
    private final RabbitTemplate template;

    private final Gson gsonBuilder;

    /**
     * Constructor for class RideMessageHandler
     *
     * @param template RabbitTemplate class used to convert and send the Message on a queue
     * @param gsonBuilder Gsonbuilder to convert VehicleLocationDTO to JSON format
     */
    public RideMessageHandler(RabbitTemplate template, Gson gsonBuilder) {
        this.template = template;
        this.gsonBuilder = gsonBuilder;
    }

    /**
     * Uses the RabbitTemplate to place the Message on a Queue
     *
     * @param timeStamp is the time of which the location of the vehicle is found as a String
     * @param vehicleID is the ID of the vehicle as a String
     * @param xCoord is the x-coordinate of the position of the vehicle as a String
     * @param yCoord is the y-coordinate of the position of the vehicle as a String
     */
    public void handleMessage(String timeStamp, String vehicleID, String xCoord, String yCoord){
        try {
            template.convertAndSend(rideQueueName,
                    gsonBuilder.toJson(
                            new VehicleLocationDTO(
                                    timeStamp,
                                    Integer.parseInt(vehicleID),
                                    Double.parseDouble(xCoord),
                                    Double.parseDouble(yCoord))));
        }catch (Exception e){
            LOGGER.warn(String.format("An error happened while trying to send messages to queue: %s", e.getMessage()));
        }
    }

    /**
     * Creates a Queue named "rideQueue" and returns it
     *
     * @return rideQueue Queue
     */
    @Bean
    public Queue rideQueue() {
        return new Queue(rideQueueName, false);
    }
}
