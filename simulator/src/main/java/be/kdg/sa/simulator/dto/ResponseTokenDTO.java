package be.kdg.sa.simulator.dto;

public class ResponseTokenDTO {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
