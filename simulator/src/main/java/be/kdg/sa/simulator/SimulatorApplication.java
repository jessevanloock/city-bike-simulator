package be.kdg.sa.simulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SimulatorApplication class to start running the simulator
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 16-09-2019
 */

@SpringBootApplication
public class SimulatorApplication{
	public static void main(String[] args) {

		SpringApplication.run(SimulatorApplication.class, args);

	}

}


