package be.kdg.sa.simulator.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public final class Utils {
    public static String getMessageFromResponseBody(String body){
        JsonParser parser = new JsonParser();
        try {
            JsonObject jsonObject = (JsonObject) parser.parse(body);
            if (jsonObject.has("message")) return jsonObject.get("message").getAsString();
            else return "No message found";
        } catch (JsonSyntaxException e) {
            return body;
        }
    }
}
