package be.kdg.sa.simulator.handlers;

import be.kdg.sa.simulator.dto.*;
import be.kdg.sa.simulator.config.SimulatorConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * RideAPIHandler class used for sending API calls to the RideService
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 07-10-2019
 */
@Component
public class RideAPIHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RideAPIHandler.class);
    private static final SimulatorConfig CONFIGURATION = new SimulatorConfig();

    @Value("${rideapi.unlock_station_vehicle}")
    private String uriUnlockStationVehicle;

    @Value("${rideapi.unlock_free_vehicle}")
    private String uriUnlockFreeVehicle;

    @Value("${rideapi.get_free_locks}")
    private String uriGetFreeLocks;

    @Value("${rideapi.lock_station_vehicle}")
    private String uriLockStationVehicle;

    @Value("${rideapi.lock_free_vehicle}")
    private String uriLockFreeVehicle;

    @Value("${rideapi.nearest_free_vehicle}")
    private String uriNearestFreeVehicle;

    @Value("${rideapi.auth}")
    private String uriAuth;

    @Value("${security.username}")
    private String username;

    @Value("${security.password}")
    private String password;

    private RestTemplate restTemplate = new RestTemplate();

    /**
     * Sends API call to RideService to unlock a station vehicle based on userID and stationID
     *
     * @param userID is the ID of the user as a String
     * @param stationID is the ID of the station as a String
     */
    public void unlockStationVehicle(int userID, int stationID)  throws HttpClientErrorException{
        LOGGER.info(String.format("Unlocking station vehicle for user (%d) at station (%d)", userID, stationID));
        StationVehicleDto stationVehicleDto = new StationVehicleDto();
        stationVehicleDto.setUserId(userID);
        stationVehicleDto.setStationId(stationID);
        String jsonFromPojo = CONFIGURATION.gson().toJson(stationVehicleDto);
        HttpHeaders headers = CONFIGURATION.headers();
        headers.add("auth", requestAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(jsonFromPojo, headers);
        Integer lockId = restTemplate.postForObject(uriUnlockStationVehicle, entity, Integer.class);
        LOGGER.info(String.format("Successfully unlocked station vehicle for user (%d) at lock (%d)", userID, lockId));
    }

    /**
     * Sends API call to RideService to unlock a free vehicle based on userID and vehicleID
     *
     * @param userID is the ID of the user as a String
     * @param vehicleID is the ID of the vehicle as a String
     */
    public void unlockFreeVehicle(int userID, int vehicleID) throws HttpClientErrorException {
        LOGGER.info(String.format("Unlocking free vehicle (%d) for user (%d)", vehicleID, userID));
        LockUnlockFreeVehicleDTO lockUnlockFreeVehicleDTO = new LockUnlockFreeVehicleDTO();
        lockUnlockFreeVehicleDTO.setUserId(userID);
        lockUnlockFreeVehicleDTO.setVehicleId(vehicleID);
        String jsonFromPojo = CONFIGURATION.gson().toJson(lockUnlockFreeVehicleDTO);
        HttpHeaders headers = CONFIGURATION.headers();
        headers.add("auth", requestAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(jsonFromPojo, headers);
        Boolean unlocked = restTemplate.postForObject(uriUnlockFreeVehicle, entity, Boolean.class);
        if (unlocked != null && unlocked) LOGGER.info(String.format("Successfully unlocked free vehicle (%d) for user (%d)", vehicleID, userID));
        else LOGGER.warn(String.format("Failed to unlock free vehicle (%d) for user (%d)", vehicleID, userID));
    }

    /**
     * Sends API call to RideService to get all free locks based on stationID
     *
     * @param stationID is the ID of the station as a String
     */
    public void getFreeLocks(int stationID) throws HttpClientErrorException{
        LOGGER.info(String.format("Retrieving free locks at station (%d)", stationID));
        final String uri = uriGetFreeLocks + stationID;
        List locks = restTemplate.getForObject(uri, List.class);
        if (locks != null && locks.size() >= 1) LOGGER.info(String.format("Successfully retrieved free locks at station (%d): %s", stationID, locks.toString()));
        else LOGGER.warn(String.format("No free locks were found at station (%d)", stationID));
    }

    /**
     * Sends API call to RideService to lock a station vehicle based on userID and lockID
     *
     * @param userID is the ID of the user as a String
     * @param lockID is the ID of the lock as a String
     */
    public void lockStationVehicle(int userID, int lockID)  throws HttpClientErrorException{
        LOGGER.info(String.format("Locking station vehicle for user (%d) at lock (%d)", userID, lockID));
        StationVehicleDto stationVehicleDto = new StationVehicleDto();
        stationVehicleDto.setUserId(userID);
        stationVehicleDto.setLockId(lockID);
        String jsonFromPojo = CONFIGURATION.gson().toJson(stationVehicleDto);
        HttpHeaders headers = CONFIGURATION.headers();
        headers.add("auth", requestAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(jsonFromPojo, headers);
        restTemplate.put(uriLockStationVehicle, entity);
        LOGGER.info(String.format("Successfully locked station vehicle for user (%d) at lock (%d)", userID, lockID));
    }

    /**
     * Sends API call to RideService to lock a free vehicle based on vehicleID and userID
     *
     * @param vehicleID is the ID of the vehicle as a String
     * @param userID is the ID of the user as a String
     */
    public void lockFreeVehicle(int userID, int vehicleID) throws HttpClientErrorException{
        LOGGER.info("Locking free vehicle ("+vehicleID+") for user ("+userID+")");
        LockUnlockFreeVehicleDTO lockUnlockFreeVehicleDTO = new LockUnlockFreeVehicleDTO();
        lockUnlockFreeVehicleDTO.setUserId(userID);
        lockUnlockFreeVehicleDTO.setVehicleId(vehicleID);
        String jsonFromPojo = CONFIGURATION.gson().toJson(lockUnlockFreeVehicleDTO);
        HttpHeaders headers = CONFIGURATION.headers();
        headers.add("auth", requestAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(jsonFromPojo, headers);
        Boolean unlocked = restTemplate.postForObject(uriLockFreeVehicle, entity, Boolean.class);
        LOGGER.info(String.format("Successfully locked free vehicle (%d) for user (%d)", vehicleID, userID));
    }

    /**
     * Sends API call to RideService to find the nearest free vehicle based on xCoord, yCoord and vehicleType
     *
     * @param xCoord is the x-coordinate of the vehicle as a String
     * @param yCoord is the y-coordinate of the vehicle as a String
     * @param vehicleType is the type of the vehicle as a String
     */
    public void findNearestFreeVehicle(double xCoord, double yCoord, String vehicleType) throws HttpClientErrorException{
        LOGGER.info(String.format("Finding nearest vehicle of type (%s) to coordinates (%f,%f)", vehicleType, xCoord, yCoord));
        FreeVehicleDto freeVehicleDto = new FreeVehicleDto();
        freeVehicleDto.setxCoord(xCoord);
        freeVehicleDto.setyCoord(yCoord);
        freeVehicleDto.setVehicleType(vehicleType);
        String jsonFromPojo = CONFIGURATION.gson().toJson(freeVehicleDto);
        HttpHeaders headers = CONFIGURATION.headers();
        headers.add("auth", requestAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(jsonFromPojo, headers);
        FreeVehicleDto foundVehicle = restTemplate.postForObject(uriNearestFreeVehicle, entity, FreeVehicleDto.class);
        if (foundVehicle != null) LOGGER.info(String.format("Found a vehicle of type (%s) at coordinates (%f,%f)", vehicleType, foundVehicle.getxCoord(), foundVehicle.getyCoord()));
        else LOGGER.warn(String.format("No vehicle of type (%s) found nearby coordinates (%f,%f)", vehicleType, xCoord, yCoord));
    }

    public String requestAccessToken() {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(username);
        loginDTO.setPassword(password);
        String jsonFromPojo = CONFIGURATION.gson().toJson(loginDTO);
        HttpEntity<String> entity = new HttpEntity<>(jsonFromPojo, CONFIGURATION.headers());
        ResponseTokenDTO tokenDTO = restTemplate.postForObject(uriAuth, entity, ResponseTokenDTO.class);
        if (tokenDTO != null) return tokenDTO.getToken();
        else LOGGER.warn("Was not able to get an access token");
        return null;
    }
}
