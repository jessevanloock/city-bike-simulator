package be.kdg.sa.simulator.controllers;

import be.kdg.sa.simulator.services.RideService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * RideController class used for displaying the views for uploading a csv and starting the ride simulation
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 4-10-2019
 */

@Controller
public class RideController {
    private final RideService rideService;
    private static final Logger LOGGER = LoggerFactory.getLogger(RideController.class);

    /**
     * Constructor for RideController
     *
     * @param rideService Rideservice used for reading the CSV and deciding which method needs to be called based on the CSV
     */
    public RideController(RideService rideService) {
        this.rideService = rideService;
    }

    /**
     * Shows the view to upload the csv
     * @return csvupload view based on String
     */
    @GetMapping("/csvupload")
    public String csvForm() {
        return "csvupload";
    }

    /**
     * Gets the uploaded csv file and calls "readCSV" from RideService
     */
    @PostMapping("/csvupload")
    @Async
    public void csvUpload(@RequestParam MultipartFile file){
        handleCsv(file);
    }

    private void handleCsv(MultipartFile file){
        BufferedReader br;
        List<String> result = new ArrayList<>();
        try {

            String line;
            InputStream is = file.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                result.add(line);
            }

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        try {
            rideService.readCSV(result);
        }catch (InterruptedException e){
            LOGGER.error(e.getMessage());
        }
    }
}
