package be.kdg.sa.simulator.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Value("${security.username}")
    private String username;
    @Value("${security.password}")
    private String password;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable() // disable csrf request support (enabled by default)
                .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login") // give everyone access to the login page
                    .permitAll()
                    .and()
                .logout()
                    .permitAll()
                .logoutSuccessUrl("/login");
    }

    @Bean
    public UserDetailsService userDetailsService() {
        // Spring web-security expects e bean of type UserDetailsService to get UserDetails
        // Since we don't have any users to authenticate, we'll use an InMemory manager to create a simple admin user
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        // Default encoder is DelegatingPasswordEncoder which requires a password storage format {noop} -> NoOpPasswordEncoder
        manager.createUser(User.withUsername(username).password("{noop}"+password).roles("ADMIN").build()); // Create an admin account
        return manager;
    }

    @Bean
    SpringSecurityDialect securityDialect() {
        return new SpringSecurityDialect();
    }
}
