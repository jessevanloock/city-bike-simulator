package be.kdg.sa.simulator.handlers;

import be.kdg.sa.simulator.model.SensorMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * SensorMessageHandler class used for creating a queue and sending messages on this queue
 * to SensorService
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 20-09-2019
 */

@Component
public class SensorMessageHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorMessageHandler.class);

    private final RabbitTemplate template;
    @Value("${sensorqueue}")
    private String sensorQueueName;

    /**
     * Constructor for class SensorMessageHandler
     *
     * @param template RabbitTemplate class used to convert and send the SensorMessage on a queue
     */
    @Autowired
    public SensorMessageHandler(RabbitTemplate template) {
        this.template = template;
    }

    /**
     * Uses the RabbitTemplate and ObjectMapper to convert the SensorMessage to a String and place
     * the String on a Queue
     *
     * @param message SensorMessage of the CSV as List<String>
     */
    public void handleMessage(SensorMessage message){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        try {
            template.convertAndSend(sensorQueueName,gson.toJson(message));
        }catch (Exception e){
            LOGGER.warn(String.format("An error happened while trying to send messages to queue: %s", e.getMessage()));
        }
    }

    /**
     * Creates a Queue named "sensorQueue" and returns it
     *
     * @return sensorQueue Queue
     */
    @Bean
    public Queue sensorQueue() {
        return new Queue(sensorQueueName, false);
    }
}
