package be.kdg.sa.simulator.services;

import be.kdg.sa.simulator.handlers.RideAPIHandler;
import be.kdg.sa.simulator.handlers.RideMessageHandler;
import be.kdg.sa.simulator.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * RideService class is a class used for reading the CSV and deciding which method needs to be called based on the CSV
 *
 * @author Jesse Van Loock
 * @version 1.0
 * @since 07-10-2019
 */

@Service
public class RideService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RideService.class);

    private final RideAPIHandler apiHandler;
    private final RideMessageHandler messageHandler;

    /**
     * Constructor for class RideService
     *
     * @param apiHandler     RideAPiHandler class used to access the api call methods
     * @param messageHandler RideMessageHandler class used to access the message handler methods
     */
    public RideService(RideAPIHandler apiHandler, RideMessageHandler messageHandler) {
        this.apiHandler = apiHandler;
        this.messageHandler = messageHandler;
    }

    /**
     * Calls chooseMethod for each line of the CSV
     *
     * @param result result of the CSV as List<String>
     */
    public void readCSV(List<String> result) throws InterruptedException {
        for (String s : result) {
            handleCsvLine(s);
        }
    }

    /**
     * Transforms each line of the csv into an Array of String and chooses right method
     * based on name of event
     *
     * @param line each line of the csv
     */
    private void handleCsvLine(String line) throws InterruptedException {
        String[] event = line.split(",");
        String name = event[0];
        try {
            switch (name.toLowerCase()) {
                case "unlockstationvehicle":
                    apiHandler.unlockStationVehicle(Integer.parseInt(event[1]), Integer.parseInt(event[2]));
                    break;
                case "unlockfreevehicle":
                    apiHandler.unlockFreeVehicle(Integer.parseInt(event[1]), Integer.parseInt(event[2]));
                    break;
                case "locationoffreevehicle":
                    messageHandler.handleMessage(event[1], event[2], event[3], event[4]);
                    break;
                case "getfreelocks":
                    apiHandler.getFreeLocks(Integer.parseInt(event[1]));
                    break;
                case "lockstationvehicle":
                    apiHandler.lockStationVehicle(Integer.parseInt(event[1]), Integer.parseInt(event[2]));
                    break;
                case "lockfreevehicle":
                    apiHandler.lockFreeVehicle(Integer.parseInt(event[1]), Integer.parseInt(event[2]));
                    break;
                case "findnearestfreevehicle":
                    apiHandler.findNearestFreeVehicle(Integer.parseInt(event[1]), Integer.parseInt(event[2]), event[3]);
            }
            TimeUnit.MILLISECONDS.sleep(Integer.parseInt(event[event.length - 1]));
        } catch (HttpClientErrorException e) {
            LOGGER.error("ERROR " + e.getStatusCode() + ": " +
                    Utils.getMessageFromResponseBody(e.getResponseBodyAsString()));
        }
    }
}
