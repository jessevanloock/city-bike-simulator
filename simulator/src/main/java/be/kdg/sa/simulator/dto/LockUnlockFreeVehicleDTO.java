package be.kdg.sa.simulator.dto;

public class LockUnlockFreeVehicleDTO {
    private int userId;
    private int vehicleId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }
}
